AC_INIT([upcxx], [0.2], [upcxx@googlegroups.com], [upcxx], [https://bitbucket.org/upcxx/upcxx])
AC_PREREQ([2.59])

AM_INIT_AUTOMAKE([1.10 no-define foreign subdir-objects])

LT_INIT([disable-shared])
dnl LT_INIT()
LT_LANG([C++])

AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_HEADERS(config.h) dnl internal config header 
AC_CONFIG_HEADERS(upcxx_config.h) dnl external config header
AC_CONFIG_FILES([
  Makefile
  include/Makefile
  include/upcxx.mak
  src/Makefile
  scripts/upc++
  examples/Makefile
  examples/basic/Makefile
  examples/cg/Makefile
  examples/gups/Makefile
  examples/knapsack/Makefile
  examples/mg/Makefile
  examples/samplesort/Makefile
  examples/spmv/Makefile
  examples/stencil/Makefile
  tests/Makefile
])

AC_PROG_CXX
AC_PROG_CC

AC_ARG_WITH([gasnet],
  [AS_HELP_STRING([--with-gasnet], [path to gasnet conduit-specific makefile])],
  [
    if test -f "$withval" ; then
      cp $withval .
      gasnet_makefile=$(basename $withval)
      chmod o+w $gasnet_makefile
      sed -i.BAK 's/-Wl,--whole-archive,-lhugetlbfs,--no-whole-archive/-lhugetlbfs/' $gasnet_makefile
      AC_SUBST([GASNET_MAKEFILE], ["$gasnet_makefile"])
      dnl AC_SUBST([GASNET_MAKEFILE], ["$withval"])
      dnl AM_CONDITIONAL([UPCXX_MPI_CONDUIT], [echo $withval | grep mpi-conduit])
      if echo $withval | grep mpi-conduit; then
        AC_SUBST([UPCXX_TESTS_MPIRUN], ["mpirun -n 4"])
      fi
    else    
      AC_MSG_FAILURE([Could not find GASNet Makefile.])
    fi
  ],
  [AC_MSG_FAILURE([No GASNet Makefile specified. Re-configure with --with-gasnet=$gasnet_install_path/mpi-conduit/mpi-seq.mak or $bupc_install_path/opt/include/smp-conduit/mpi-seq.mak])]
)

AC_LANG(C++)

dnl option to disable using C++14  (default is enable auto detection)
AC_ARG_ENABLE([cxx14],
    AS_HELP_STRING([--disable-cxx14], [Disable C++14 features]))

AS_IF([test "x$enable_cxx14" = "xno"],
      [:], 
      [ dnl Do the stuff needed for enabling C++14
        dnl Check for C++14   
        AX_CXX_COMPILE_STDCXX(14, noext,optional)
        if test "$HAVE_CXX14" = 1; then
          AC_DEFINE(UPCXX_HAVE_CXX14, 1, [define if the C++14 features are enabled])
          AC_SUBST(UPCXX_HAVE_CXX14)
          AC_DEFINE(UPCXX_HAVE_CXX11, 1, [define if the C++11 features are enabled])
          AC_SUBST(UPCXX_HAVE_CXX11)
          AC_SUBST(CXX14FLAG)
        fi
      ])

if test "HAVE_CXX14" != 1; then
  dnl option to disable using C++11  (default is enable auto detection)
  AC_ARG_ENABLE([cxx11],
      AS_HELP_STRING([--disable-cxx11], [Disable C++11 features]))

  AS_IF([test "x$enable_cxx11" = "xno"],
        [:], 
        [ dnl Do the stuff needed for enabling C++11
          dnl Check for C++11   
          AX_CXX_COMPILE_STDCXX_11(noext,optional)
          if test "$HAVE_CXX11" = 1; then
            AC_DEFINE(UPCXX_HAVE_CXX11, 1, [define if the C++11 features are enabled])
            AC_SUBST(UPCXX_HAVE_CXX11)
            AC_SUBST(CXX11FLAG)
          fi
        ])
fi


dnl Check for support for is_trivially_destructible
AC_MSG_CHECKING([whether $CXX supports std::is_trivially_destructible])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
    #include <type_traits>
    bool test = std::is_trivially_destructible<double>::value;
  ])], [TRIVIALDESTRUCT=yes], [TRIVIALDESTRUCT=no])
AC_MSG_RESULT([$TRIVIALDESTRUCT])
if test "x$TRIVIALDESTRUCT" = "xyes"; then
  AC_DEFINE(UPCXX_HAVE_TRIVIALLY_DESTRUCTIBLE, 1, [define if libcxx implements std::is_trivially_destructible])
fi

dnl Check for support for initializer lists
AC_MSG_CHECKING([whether $CXX supports std::initializer_list])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
    #include <initializer_list>
    void test(std::initializer_list<double> &ilist) {}
  ])], [INITIALIZERLIST=yes], [INITIALIZERLIST=no])
AC_MSG_RESULT([$INITIALIZERLIST])
if test "x$INITIALIZERLIST" = "xyes"; then
 AC_DEFINE(UPCXX_HAVE_INITIALIZER_LIST, 1, [define if libcxx implements std::initializer_list])
fi

dnl Check for Clang
AC_MSG_CHECKING([if compiling with clang])
AC_COMPILE_IFELSE(
[AC_LANG_PROGRAM([], [[
#ifndef __clang__
       not clang
#endif
]])], [CLANG=yes], [CLANG=no])
AC_MSG_RESULT([$CLANG])
dnl for clang++
if test "x$CLANG" = "xyes"; then
  ## suppress warnings due to legacy usage of C in GASNet
  UPCXX_FLAGS+=" -Wno-reserved-user-defined-literal -Wno-deprecated-register"
fi

dnl Check for GCC
AC_MSG_CHECKING([if compiling with g++])
AC_COMPILE_IFELSE(
[AC_LANG_PROGRAM([], [[
#if !defined(__GNUG__) || defined(__INTEL_COMPILER) || defined(__clang__) || defined(__PGI)
       not g++
#endif
]])], [GCC=yes], [GCC=no])
AC_MSG_RESULT([$GCC])
dnl for g++
if test "x$GCC" = "xyes"; then
  ## suppress warnings due to legacy usage of C in GASNet
  UPCXX_FLAGS+=" -Wno-literal-suffix -DGASNETT_USE_GCC_ATTRIBUTE_MAYALIAS=1 "
fi

dnl Add flags for building UPC++
UPCXX_FLAGS+=" -DGASNET_ALLOW_OPTIMIZED_DEBUG "

dnl compilation flags for CD
AC_ARG_ENABLE([cd],
    AS_HELP_STRING([--enable-cd], [Enable CD runtime for resilience]))

CD_FLAGS=""
AS_IF([test "x$enable_cd" = "xyes"], [
  AC_DEFINE(CDENABLED, 1, [define to enable CD feature])
  AC_SUBST(CDENABLED)
  CD_FLAGS=" -I"${prefix}"/cdruntime/src -DCDENABLED -D_LOG_PROFILING=1 -fmax-errors=5 -fPIC -std=gnu++0x -I${BOOST_DIR}/include "
  AC_ARG_ENABLE([debug],
                [AS_HELP_STRING([--enable-debug=ARG], [ARG is file/stdout])],
                [], enable_debug=no)
  AS_IF([test "x$enable_debug" = x"file"], 
        [
         WANT_DEBUG=1
         CD_FLAGS+="-g "
        ],
        [test "x$enable_debug" = x"stdout"], 
        [
         WANT_DEBUG=2
         CD_FLAGS+="-g "
        ],
        [test "x$enable_debug" = x"no"], 
        [
         WANT_DEBUG=0
        ],
        AC_MSG_FAILURE([--enable-debug must be file/stdout/no]))

  AC_DEFINE_UNQUOTED(_DEBUG, $WANT_DEBUG,
            [Define to 1 to enable debugging output])
  AC_DEFINE_UNQUOTED(_CD_DEBUG, $WANT_DEBUG,
            [Define to 1 for debugging file output, 2 for stdout])
  AC_SUBST(cd_debug_enabled, $WANT_DEBUG)
  AC_SUBST(cd_debug_dest, $WANT_DEBUG)


  # profiling support
  AC_ARG_ENABLE([profiler], 
      [AS_HELP_STRING([--enable-profiler],[Turn on profiler support])])
  AS_IF([test x"$enable_profiler" = x"yes"], [WANT_PROFILER=1], [WANT_PROFILER=0])
  AC_DEFINE_UNQUOTED(_PROFILER, $WANT_PROFILER, [Define to 1 to enable profiler])
  AC_SUBST(cd_profiler_enabled, $WANT_PROFILER)
  
  # profiling support
  AC_ARG_ENABLE([viz], 
      [AS_HELP_STRING([--enable-viz],[Turn on visualization])])
  AS_IF([test x"$enable_viz" = x"yes"], [WANT_VIZ=1], [WANT_VIZ=0])
  AC_DEFINE_UNQUOTED(_SIGHT, $WANT_VIZ, [Define to 1 to enable visualization])
  AC_SUBST(cd_viz_enabled, $WANT_VIZ)
  dnl # some additonal preprocessor symbols to configure profiling
  dnl # XXX: I have no idea what these do, so please update the descriptions!
  dnl AC_DEFINE(_ENABLE_MODULE, [1], [Define to 1 for Module])
  dnl AC_DEFINE(_ENABLE_HIERGRAPH, [0], [Define to 1 for CD Hierarchical Graph App])
  dnl AC_DEFINE(_ENABLE_SCOPE, [0], [Define to 1 to profile Scope])
  dnl # XXX: Old Makefile used _ENABLE_GRAPH, but code used _ENABLE_SCOPEGRAPH
  dnl AC_DEFINE(_ENABLE_SCOPEGRAPH, [0], [Define to 1 for ScopeGraph])
  dnl AC_DEFINE(_ENABLE_ATTR, [0], [Define to 1 for Attribute])
  dnl AC_DEFINE(_ENABLE_COMP, [0], [Define to 1 for Comparison])
  dnl AC_DEFINE(_ENABLE_CDNODE, [0], [Define to 1 for CDNode])
  
  # comm_log is tested via ifdef, so this one is conditionally defined
  #
  # logging (seems to refer only to communications logging.)
  # XXX: Rename?
  AC_ARG_ENABLE([logging], 
                [AS_HELP_STRING([--enable-logging], [Enable message logging (default is on)])],
                [], enable_logging=yes)
  AS_IF([test x"$enable_logging" = x"yes"], 
  [
      WANT_LOGGING=1
      AC_DEFINE(comm_log, [1], [Define to enable communications logging])
  ],[
      WANT_LOGGING=0
  ])
  AC_SUBST(cd_comm_log_enabled, $WANT_LOGGING)
  
  # libc logging
  # Also checked via #ifdef, so our AC_DEFINE is conditional.
  # XXX:  Change these flags to be consistent with others!
  AC_ARG_ENABLE([libc-logging], 
                [AS_HELP_STRING([--enable-libc-logging], [Enable libc logging (default is on)])],
                [], enable_libc_logging=yes)
  AS_IF([test x"$enable_libc_logging" = x"yes"], 
  [
      WANT_LIBC_LOGGING=1
      AC_DEFINE(libc_log, [1], [Define to enable libc logging])
  ],[
      WANT_LIBC_LOGGING=0
  ])
  AC_SUBST(cd_libc_log_enabled, $WANT_LIBC_LOGGING)
  
  dnl Test enable 
  AC_ARG_ENABLE([cd_test], 
      [AS_HELP_STRING([--enable-cd-test], [Turn on internal test mode])])
  AS_IF([test x"$enable_cd_test" = x"yes"], 
  [
      WANT_CD_TEST=1
  ],[
      WANT_CD_TEST=0
  ])
  AC_SUBST(cd_test_enabled, $WANT_CD_TEST)
  
  # error injection
  AC_ARG_ENABLE([error-injection], 
      [AS_HELP_STRING([--enable-error-injection], [Turn on error injection])])
  AS_IF([test x"$enable_error_injection" = x"yes"], 
          [WANT_ERROR_INJECTION=1], [WANT_ERROR_INJECTION=0])
  AC_DEFINE_UNQUOTED(_ERROR_INJECTION_ENABLED, $WANT_ERROR_INJECTION,
            [Define to 1 to enable error injection])
  AC_SUBST(cd_error_injection_enabled, $WANT_ERROR_INJECTION)
  
  # keep preservation files.
  # XXX: Should this be a runtime option?
  AC_ARG_ENABLE([keep-preservation-files],
      [AS_HELP_STRING([--enable-keep-preservation-files], 
      [Keep preservation files rather than deleting them.])])
  AS_IF([test x"$enable_keep_preservation_files" = x"yes"], 
          [WANT_KEEP_PRESERVATION_FILES=1], [WANT_KEEP_PRESERVATION_FILES=0])
  AC_DEFINE_UNQUOTED(_PRV_FILE_NOT_ERASED, $WANT_ERROR_INJECTION,
            [Define to 1 to keep preservation files after program exits.])
  AC_DEFINE_UNQUOTED(_PRV_FILE_BASEPATH, $WANT_ERROR_INJECTION,
            [Define to 1 to keep preservation files after program exits.])
  
  # parallel library support
  WANT_SINGLE=0
  WANT_PGAS=1
  WANT_MPI=0
  AC_DEFINE_UNQUOTED(_MPI_VER, $WANT_MPI, [Define to 1 for MPI support.])
  AC_DEFINE_UNQUOTED(_PGAS_VER, $WANT_PGAS, [Define to 1 for PGAS support.])
  AC_DEFINE_UNQUOTED(_SINGLE_VER, $WANT_SINGLE, [Define to 1 for serial library])
  AC_SUBST(cd_mpi_enabled, $WANT_MPI)
  AC_SUBST(cd_pgas_enabled, $WANT_PGAS)

  UPCXX_FLAGS+=${CD_FLAGS}
  UPCXX_LDLIBS_EXTRA=" -L${BOOST_DIR}/lib -lboost_serialization "


  AC_CONFIG_HEADERS([cdruntime/src/cd_config.h])
  AC_CONFIG_FILES([
   cdruntime/src/cd_features.h
  ])
  ## FIXME: should include following into target config files
   #cdruntime/test/Makefile
   #cdruntime/test/test_simple_hierarchy/Makefile
   #cdruntime/test/test_user_defined_hierarhcy/Makefile
   #cdruntime/test/test_prv_ref_remote/Makefile
   #cdruntime/test/test_serdes/Makefile
   #cdruntime/examples/Makefile

]) ## end of cd configure

AM_CONDITIONAL([CDENABLED], [test "x$enable_cd" = "xyes"])

AC_SUBST(UPCXX_FLAGS)
AC_SUBST(UPCXX_LDLIBS_EXTRA)

CFLAGS+=${UPCXX_FLAGS}
CXXFLAGS+=${UPCXX_FLAGS}

dnl Option to enable shorter UPC++ macro names (default is disable)
AC_ARG_ENABLE([short-names],
    AS_HELP_STRING([--enable-short-names], [Enable shorter names for common UPC++ keywords]))

AS_IF([test "x$enable_short_names" = "xyes"], [
  dnl Do the stuff needed for enabling md-array
  AC_DEFINE(UPCXX_SHORT_MACROS, 1, [define if short-names feature is enabled])
  AC_SUBST(UPCXX_SHORT_MACROS)
])

dnl Option to enable UPC++ multidimensional arrays (default is disable)
AC_ARG_ENABLE([md-array],
    AS_HELP_STRING([--enable-md-array], [Enable Titanium-style multidimensional arrays]))

AS_IF([test "x$enable_md_array" = "xyes"], [
  dnl Do the stuff needed for enabling md-array
  AC_DEFINE(UPCXX_HAVE_MD_ARRAY, 1, [define if md-array feature is enabled])
  AC_SUBST(UPCXX_HAVE_MD_ARRAY)
])

AM_CONDITIONAL([UPCXX_MD_ARRAY], [test "x$enable_md_array" = "xyes"])

dnl Option to enable using the Cray DMAPP API on Aries network (default is disable)
AC_ARG_ENABLE([dmapp],
    AS_HELP_STRING([--enable-dmapp], [Enable using DMAPP on Cray Aries network]))

AS_IF([test "x$enable_dmapp" = "xyes"], [
  dnl Do the stuff needed for enabling dmapp
  AC_DEFINE(UPCXX_USE_DMAPP, 1, [define if dmapp feature is enabled])
  AC_SUBST(UPCXX_USE_DMAPP)
])

AM_CONDITIONAL([UPCXX_USE_DMAPP], [test "x$enable_dmapp" = "xyes"])

dnl Option to enable UPC++ thread safety (default is disable)
AC_ARG_ENABLE([thread-safe],
    AS_HELP_STRING([--enable-thread-safe], [Enable thread safety for the UPC++ runtime]))

AS_IF([test "x$enable_thread_safe" = "xyes"], [
  dnl Do the stuff needed for enabling thread safety
  AC_DEFINE(UPCXX_THREAD_SAFE, 1, [define if thread safety is enabled])
  AC_SUBST(UPCXX_THREAD_SAFE)
])

AM_CONDITIONAL([UPCXX_THREAD_SAFE], [test "x$enable_thread_safe" = "xyes"])

dnl Option to disable 64-bit global pointer  (default is enable)
AC_ARG_ENABLE([64bit-global-ptr],
    AS_HELP_STRING([--enable-64bit-global-ptr], [Enable 64-bit global pointer representation]))

AS_IF([test "x$enable_64bit_global_ptr" = "xyes"], [
  dnl Do the stuff needed for enabling 64-bit global pointer representation 
  AC_DEFINE(UPCXX_USE_64BIT_GLOBAL_PTR, 1, [define if 64-bit global pointer representation is enabled])
  AC_SUBST(UPCXX_USE_64BIT_GLOBAL_PTR)
])

AM_CONDITIONAL([UPCXX_USE_64BIT_GLOBAL_PTR], [test "x$enable_64bit_global_ptr" = "xyes"])

AC_OUTPUT
