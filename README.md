### UPC\+\+ CD: a PGAS extension for Containment Domains ###

## **Containment Domains Introduction** ##

Containment Domains (CDs) is a library-level approach dealing with low-overhead resilient and scalable execution (see [http://lph.ece.utexas.edu/public/CDs](http://lph.ece.utexas.edu/public/CDs)). CDs abandon the prevailing one-size-fits-all approach to resilience and instead embrace the diversity of application needs, resilience mechanisms, and the deep hierarchies expected in exascale hardware and software.  CDs give software a means to express resilience concerns intuitively and concisely.  With CDs, software can preserve and restore state in an optimal way within the storage hierarchy  and can efficiently support uncoordinated recovery.  In addition, CDs allow software to tailor error detection, elision (ignoring some errors), and recovery mechanisms to algorithmic and system needs.

[The documentation about CD API](http://lph.ece.utexas.edu/users/CDAPI/) is available online. 

Please see the [Containment Domains](http://lph.ece.utexas.edu/public/CDs/ContainmentDomains) for more info.


## **CDs Media** ##

* [CDs Demo for MPI Program](https://www.youtube.com/watch?v=1JKAu7HzM08)
* [CDs Demo for PGAS Program](https://www.youtube.com/watch?v=6JwjGbT4NVw)


## **UPC\+\+ Info** ##

UPC++ is a parallel programming extension for developing C++ applications with the partitioned global address space (PGAS) model.  UPC++ has three main objectives: 

* Provide an object-oriented PGAS programming model in the context of the popular C++ language
* Add useful parallel programming idioms unavailable in Unified Parallel C (UPC), such as asynchronous remote function invocation and multidimensional arrays, to support complex scientific applications
* Offer an easy on-ramp to PGAS programming through interoperability with other existing parallel programming systems (e.g., MPI, OpenMP, CUDA)

Please see the [UPC++ wiki](https://bitbucket.org/upcxx/upcxx/wiki) for more info.


## **UPC\+\+ Media** ##

UPC++ enabled new seismic research insights

* [Origins of Volcanic Island Chains](https://youtu.be/tCphzt8iaWc)
* [CT Scan of Earth Links Mantle Plumes with Volcanic Hotspots](http://cs.lbl.gov/news-media/news/2015/ct-scan-of-earth-links-mantle-plumes-with-volcanic-hotspots/)
* [NERSC 2016 Award for Innovative Use of HPC (Scott French)](http://www.nersc.gov/news-publications/nersc-news/nersc-center-news/2015/nersc-announces-4th-annual-hpc-achievement-award-winners/)
