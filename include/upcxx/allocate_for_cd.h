/**
 * allocate_for_cd.h - provide memory allocation service for the GASNet segment
 */

#ifdef CDENABLED
#pragma once

#include "global_ptr_for_cd.h"

namespace upcxx
{
  /**
   * @ingroup gasgroup
   * @brief allocate_for_cd memory in the global address space of a rank (process)
   *
   * @return the pointer to the allocated pace
   * @param nbytes the number of element to be copied
   * @param rank the rank_t where the memory space should be allocated
   */
  global_ptr_for_cd<void> allocate_for_cd(rank_t rank, size_t nbytes);

  static inline void* allocate_for_cd(size_t nbytes) 
  {
    return allocate_for_cd(global_myrank(), nbytes).raw_ptr();
  }

  /**
   * @ingroup gasgroup
   * @brief allocate_for_cd memory in the global address space at a specific rank_t
   *
   * \tparam T type of the element
   *
   * @return the pointer to the allocated pace
   * @param count the number of element to be copied
   * @param rank the rank_t where the memory space should be allocated
   */
  template<typename T>
  global_ptr_for_cd<T> allocate_for_cd(rank_t rank, size_t count)
  {
    size_t nbytes = count * sizeof(T);
    return global_ptr_for_cd<T>(allocate_for_cd(rank, nbytes));
  }

  template<typename T>
  T* allocate_for_cd(size_t count)
  {
    size_t nbytes = count * sizeof(T);
    return (T*)allocate_for_cd(nbytes);
  }

  void deallocate_for_cd(global_ptr_for_cd<void> ptr);

  void deallocate_for_cd(void *ptr);

  /**
   * @ingroup gasgroup
   * @brief free memory in the global address space
   *
   * \tparam T type of the element
   *
   * @param ptr the pointer to which the memory space should be freed
   */
  template<typename T>
  void deallocate_for_cd(global_ptr_for_cd<T> ptr)
  {
    deallocate_for_cd(global_ptr_for_cd<void>(ptr));
  }
} // namespace upcxx
#endif
