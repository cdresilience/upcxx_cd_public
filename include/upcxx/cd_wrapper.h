/*
Copyright 2014, The University of Texas at Austin 
All rights reserved.

THIS FILE IS PART OF THE CONTAINMENT DOMAINS RUNTIME LIBRARY

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met: 

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution. 

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef CDENABLED

#pragma once

#include "event.h"
#include "gasnet_api.h"

///* for definition of MYTHREAD*/
//#include "team.h"

//using namespace cd;
//using namespace cd::logging;

namespace upcxx{

  // blocking gets
  void cd_gasnet_get(void *dest, gasnet_node_t node, void *src, size_t nbytes, int skip_waiting=0, size_t* done=NULL);
  void cd_gasnet_get_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes, int skip_waiting=0, size_t* done=NULL);
  void cd_gasnet_wait_for_done(size_t *done);

  // non-blocking gets
  gasnet_handle_t cd_gasnet_get_nb_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes, size_t *done=NULL);
  void cd_gasnet_get_nbi_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes, size_t* done=NULL);

  // blocking puts
  void cd_gasnet_put(gasnet_node_t node, void *dest, void *src, size_t nbytes, int skip_waiting=0, size_t *done=NULL);
  void cd_gasnet_put_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes, int skip_waiting=0, size_t *done=NULL);

  // non-blocking puts
  gasnet_handle_t cd_gasnet_put_nb_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes, size_t* done=NULL);
  void cd_gasnet_put_nbi_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes, size_t* done=NULL);
  void cd_gasnet_put_nbi(gasnet_node_t node, void *dest, void *src, size_t nbytes, size_t* done=NULL);
  
  // puts/gets fence
  int cd_gasnet_try_syncnb_nopoll(gasnet_handle_t handle, int log_event, event* pevent);
  int cd_gasnet_try_syncnb_nopoll_log_event(event* pevent);
  int cd_gasnet_try_syncnbi_all();
  int cd_check_nbi_status_all();
  void cd_gasnet_wait_syncnbi_all();
  void cd_wait_nbi_status_all();

  // collectives
  void cd_gasnet_coll_reduce(gasnet_team_handle_t team,
                             gasnet_image_t dstimage, void *dst,
                             void *src, size_t src_blksz, size_t src_offset,
                             size_t elem_size, size_t elem_count,
                             gasnet_coll_fn_handle_t func, int func_arg,
                             int flags);
  
  void cd_gasnet_coll_broadcast(gasnet_team_handle_t team,
                                void *dst,
                                gasnet_image_t srcimage, void *src,
                                size_t nbytes, int flags GASNETE_THREAD_FARG);
  
  void cd_gasnet_coll_gather(gasnet_team_handle_t team,
                             gasnet_image_t dstimage, void *dst,
                             void *src,
                             size_t nbytes, int flags);
  
  void cd_gasnet_coll_gather_all(gasnet_team_handle_t team,
                             void *dst, void *src,
                             size_t nbytes, int flags);
  
  void cd_gasnet_coll_exchange(gasnet_team_handle_t team,
                               void *dst, void *src,
                               size_t nbytes, int flags);
  
  void cd_gasnet_coll_barrier_notify(gasnet_team_handle_t team,
                                     int id,
                                     int flags);
  
  int cd_gasnet_coll_barrier_try(gasnet_team_handle_t team,
                                 int id,
                                 int flags);
  
  // misc
  void cd_reissue_puts_gets(void); // re-issue NACKed puts/gets
  void cd_issue_rdma_ops(void); // used to issue RDMA ops when payloads exceed AM_REQ_LONG size..
  void cd_issue_rdma_ops_for_cd(void); // used to issue RDMA ops when payloads exceed AM_REQ_LONG size..
  void cd_unpack_write_logs(void* addr, size_t nbytes, size_t dst_rank, void* done);
}

#endif
