/**
 * atomic_for_cd.h - atomic_for_cd operations
 *
 * This is currently an experimental feature for preview.
 */

#ifdef CDENABLED
#pragma once

#include "gasnet_api.h"
#include "event_for_cd.h"
#include "global_ptr_for_cd.h"

#include <iostream>


namespace upcxx
{
  template<typename T>
  struct atomic_for_cd {
  public:
    inline atomic_for_cd(const T& data) : _data(data)
    { }

    inline atomic_for_cd() : _data()
    { }

    inline T load() { return _data; }
    inline void store(const T& val) { _data = val; }

    inline T fetch_add(const T& add_val)
    {
      // no threading
      T old = _data;
      _data += add_val;
      return old;
    }
    
  private:
    T _data;
    // mutex
  };

  template<typename T>
  struct fetch_add_am_t_for_cd {
    atomic_for_cd<T> *obj;
    T add_val;
    T* old_val_addr;
    event_for_cd* cb_event;
  };

  template<typename T>
  struct fetch_add_reply_t_for_cd {
    T old_val;
    T* old_val_addr;
    event_for_cd* cb_event;
  };

  #ifdef UPCXX_HAVE_CXX11
  template<typename T = uint64_t>
  #else
  template<typename T>
  #endif
  static void fetch_add_am_handler_for_cd(gasnet_token_t token, void *buf, size_t nbytes)
  {
    fetch_add_am_t_for_cd<T> *am = (fetch_add_am_t_for_cd<T> *)buf;
    fetch_add_reply_t_for_cd<T> reply;

    // if no threading
    reply.old_val = am->obj->fetch_add(am->add_val);
    reply.old_val_addr = am->old_val_addr;
    reply.cb_event = am->cb_event; // callback event_for_cd on the src rank
    // YZ: should use a different GASNet AM handler id for different type T
    GASNET_SAFE(gasnet_AMReplyMedium0(token, FETCH_ADD_U64_REPLY_FOR_CD,
                                      &reply, sizeof(reply)));
  }

  #ifdef UPCXX_HAVE_CXX11
  template<typename T = uint64_t>
  #else
  template<typename T>
  #endif
  static void fetch_add_reply_handler_for_cd(gasnet_token_t token, void *buf, size_t nbytes)
  {
    fetch_add_reply_t_for_cd<T> *reply = (fetch_add_reply_t_for_cd<T> *)buf;

    *reply->old_val_addr = reply->old_val;
    reply->cb_event->decref();
  }

  /**
   * atomic_for_cd fetch-and-add: atomically add val to the atomic_for_cd obj and
   * then return the old value of the atomic_for_cd object
   */
  inline
  uint64_t fetch_add_for_cd(global_ptr_for_cd<atomic_for_cd<uint64_t> > obj, uint64_t add_val)
  {
    event_for_cd e;
    uint64_t old_val;
    fetch_add_am_t_for_cd<uint64_t> am;

    gasnet_AMPoll(); // process other AMs to be fair

    am.obj = obj.raw_ptr();
    am.add_val = add_val;
    am.old_val_addr = &old_val;
    am.cb_event = &e;
    e.incref();
    
    // YZ: should use a different GASNet AM handler id for different type T
    GASNET_SAFE(gasnet_AMRequestMedium0(obj.where(), FETCH_ADD_U64_AM_FOR_CD, &am, sizeof(am)));
    e.wait();

    gasnet_AMPoll(); // process other AMs to be fair

    return old_val;
  }

#if GASNETC_GNI_FETCHOP
  inline uint64_t my_fetch_add_for_cd(global_ptr_for_cd<uint64_t> obj, uint64_t add_val)
  {
    uint64_t old_val;

    old_val = gasnetX_fetchadd_u64_val(obj.where(), obj.raw_ptr(), add_val);

    return old_val;
  }
#endif

  /*------------------------------------ For CD ----------------------------------------*/

  template<typename T>
  struct bor_am_t_for_cd {
    atomic_for_cd<T> *obj;
    T bor_val;
    event_for_cd* cb_event;
  };

  template<typename T>
  struct bor_reply_t_for_cd {
    event_for_cd* cb_event;
  };

  #ifdef UPCXX_HAVE_CXX11
  template<typename T = uint64_t>
  #else
  template<typename T>
  #endif
  static void bor_am_handler_for_cd(gasnet_token_t token, void *buf, size_t nbytes)
  {
    bor_am_t_for_cd<T> *am = (bor_am_t_for_cd<T> *)buf;
    bor_reply_t_for_cd<T> reply;

    // if no threading
    T tmp_data = am->obj->load();
    tmp_data |= am->bor_val;
    am->obj->store(tmp_data);
    reply.cb_event = am->cb_event; // callback event_for_cd on the src rank
    // YZ: should use a different GASNet AM handler id for different type T
    GASNET_SAFE(gasnet_AMReplyMedium0(token, BOR_U64_REPLY_FOR_CD,
                                      &reply, sizeof(reply)));
  }

  #ifdef UPCXX_HAVE_CXX11
  template<typename T = uint64_t>
  #else
  template<typename T>
  #endif
  static void bor_reply_handler_for_cd(gasnet_token_t token, void *buf, size_t nbytes)
  {
    bor_reply_t_for_cd<T> *reply = (bor_reply_t_for_cd<T> *)buf;

    reply->cb_event->decref();
  }

  /**
   * atomic_for_cd fetch-and-add: atomically add val to the atomic_for_cd obj and
   * then return the old value of the atomic_for_cd object
   */
  inline
  void bor_for_cd(global_ptr_for_cd<atomic_for_cd<uint64_t> > obj, uint64_t bor_val)
  {
    event_for_cd e;
    bor_am_t_for_cd<uint64_t> am;

    gasnet_AMPoll(); // process other AMs to be fair

    am.obj = obj.raw_ptr();
    am.bor_val = bor_val;
    am.cb_event = &e;
    e.incref();
    
    // YZ: should use a different GASNet AM handler id for different type T
    GASNET_SAFE(gasnet_AMRequestMedium0(obj.where(), BOR_U64_AM_FOR_CD, &am, sizeof(am)));
    e.wait();

    gasnet_AMPoll(); // process other AMs to be fair

    return;
  }
} // end of upcxx
#endif
