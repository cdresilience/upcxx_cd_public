/**
 * event_for_cd.h - Event object for CD internal usage
 */

#pragma once

#include <iostream>
#include <cassert>
#include <vector>
#include <list>

#include <stdio.h>
#include <stdlib.h>

#include "gasnet_api.h"
#include "queue.h"
#include "upcxx_runtime.h"

namespace upcxx
{
  struct async_task; // defined in async_gasnet.h

  // extern gasnet_hsl_t async_lock;
  // extern queue_t *async_task_queue;
  extern upcxx_mutex_t in_task_queue_lock;
  extern queue_t *in_task_queue;
  extern upcxx_mutex_t out_task_queue_lock;
  extern queue_t *out_task_queue;
  extern upcxx_mutex_t all_events_lock;

#define MAX_NUM_DONE_CB 16

#define USE_EVENT_LOCK

  /**
   * \addtogroup asyncgroup Asynchronous task execution and Progress
   * @{
   * Events are used to notify asynch task completion and invoke callback functions.
   */
  struct event_for_cd {
    volatile int _count; // outstanding number of tasks.
    int owner;
    std::vector<gasnet_handle_t> _gasnet_handles;
#ifdef UPCXX_USE_DMAPP
    std::vector<dmapp_syncid_handle_t> _dmapp_handles;
#endif
    void *_rv_ptr; // pointer to the return value
    size_t _rv_sz; // size of the return value
    int _num_done_cb;
    async_task *_done_cb[MAX_NUM_DONE_CB];  
    // std::vector<async_task &> _cont_tasks;

    inline event_for_cd(void *rv_ptr = NULL, size_t rv_sz = 0)
      : _count(0), _num_done_cb(0), owner(0), _rv_ptr(rv_ptr), _rv_sz(rv_sz)
    {
    }

    inline ~event_for_cd()
    {
      this->wait();
    }

    inline int count() const { return _count; }

    void _enqueue_cb();

    inline bool isdone() const
    {
      assert(_count >= 0);
      return (_count == 0);
    }
      
    // Increment the reference counter for the event_for_cd
    void incref(uint32_t c=1)
    {
      upcxx_mutex_lock(&all_events_lock);
      _incref(c);
      upcxx_mutex_unlock(&all_events_lock);
    }


    // Decrement the reference counter for the event_for_cd
    inline void decref(uint32_t c=1)
    {
      upcxx_mutex_lock(&all_events_lock);
      _decref(c);
      upcxx_mutex_unlock(&all_events_lock);
    }

    inline void add_gasnet_handle(gasnet_handle_t h)
    {
      upcxx_mutex_lock(&all_events_lock);
      _add_gasnet_handle(h);
      upcxx_mutex_unlock(&all_events_lock);
    }

    void _add_gasnet_handle(gasnet_handle_t h);

#ifdef UPCXX_USE_DMAPP
    void add_dmapp_handle(dmapp_syncid_handle_t h);
#endif

    inline int num_done_cb() const { return _num_done_cb; }

    void _add_done_cb(async_task *task);

    inline void add_done_cb(async_task *task)
    {
      upcxx_mutex_lock(&all_events_lock);
      _add_done_cb(task);
      upcxx_mutex_unlock(&all_events_lock);
    }

    /**
     * Wait for the asynchronous event_for_cd to complete
     */
    void wait(); 

    /**
     * Return 1 if the task is done; return 0 if not
     */
    inline int async_try()
    {
      if (upcxx_mutex_trylock(&all_events_lock) != 0) {
        return isdone(); // somebody else is holding the lock
      }
      int rv = _async_try();
      upcxx_mutex_unlock(&all_events_lock);
      return rv;
    }

    inline int test() { return async_try(); }

  private:
    void _decref(uint32_t c=1);
    void _incref(uint32_t c=1);
    int _async_try();
    friend int advance_for_cd(int max_in, int max_out, int log_event);
    friend int advance(int max_in, int max_out, int log_event);
  }; //struct event_for_cd
  /// @}

  //void event_incref(event_for_cd *e, uint32_t c=1);
  //void event_decref(event_for_cd *e, uint32_t c=1);

  //inline
  //std::ostream& operator<<(std::ostream& out, const event_for_cd& e)
  //{
  //  return out << "event_for_cd: count " << e.count()
  //             << ", # of callback tasks " << e.num_done_cb()
  //             << "\n";
  //}

  extern event_for_cd *system_event_for_cd; // defined in upcxx.cpp
  extern std::list<event_for_cd *> *outstanding_events_for_cd;

  ///* event_for_cd stack interface used by finish */
  //void push_event(event_for_cd *);
  //void pop_event();
  //event_for_cd *peek_event();

  //struct event_stack {
  //  std::vector<event_for_cd *> stack;
  //  inline event_stack() {
  //    stack.push_back(system_event_for_cd);
  //  }
  //};
  //extern event_stack *events;

  inline void async_wait_for_cd(event_for_cd *e)
  {
    if (e != NULL) {
      e->wait();
    }
  }

  void async_wait_for_cd();

  int async_try_for_cd(event_for_cd *e);

} // namespace upcxx
