/*
 * active_message_for_cd.h - implement a wrapper of GASNet active messages for CD runtime to use
 */

#pragma once

#ifdef CDENABLED
namespace upcxx{
  void am_log_request(rank_t dst_rank, size_t *done);
  void am_log_request_sync(size_t num, size_t *done);
  void am_release_blocked_threads(size_t dst_rank);
}
#endif

