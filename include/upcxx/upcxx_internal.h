/*
 * upcxx_internal.h -- UPC++ runtime internal interface, which should only be
 * used inside UPC++ runtime implementation but not in any UPC++ applications
 */

#pragma once

#define ONLY_MSPACES 1 // only use mspace from dl malloc
#include "dl_malloc.h"

#include <stdlib.h>
#include <assert.h>
#include "gasnet_api.h"

namespace upcxx 
{
  /// \cond SHOW_INTERNAL
  struct alloc_am_t
  {
    size_t nbytes;
    void **ptr_addr;
    event *cb_event;
  };

  struct alloc_reply_t
  {
    void **ptr_addr;
    void *ptr;
    event *cb_event;
  };

  struct free_am_t
  {
    void *ptr;
  };

  struct inc_am_t
  {
    void *ptr;
  };
  /// @endcond SHOW_INTERNAL

  /**
   * @ingroup internal
   * Advance the outgoing task queue by processing local tasks
   *
   * Note that some local tasks may take
   *
   * @param max_dispatched the maximum number of tasks to be processed
   *
   * @return the number of tasks that have been processed
   */
  int advance_out_task_queue(queue_t *outq, int max_dispatched);

  inline int advance_out_task(int max_dispatched = MAX_DISPATCHED_OUT)
  {
    return advance_out_task_queue(out_task_queue, max_dispatched);
  }

  /*
   * @ingroup internal
   * Advance the incoming task queue by sending out remote task requests
   *
   * Note that advance_out_task_queue() shouldn't be be called in
   * any GASNet AM handlers because it calls gasnet_AMPoll() and
   * may result in a deadlock.
   *
   * @param max_dispatched the maximum number of tasks to send
   *
   * @return the number of tasks that have been sent
   */
  int advance_in_task_queue(queue_t *inq, int max_dispatched);

  inline int advance_in_task(int max_dispatched = MAX_DISPATCHED_IN)
  {
    return advance_in_task_queue(in_task_queue, max_dispatched);
  }
  
  // AM handler functions
  void async_am_handler(gasnet_token_t token, void *am, size_t nbytes);
  void async_done_am_handler(gasnet_token_t token, void *am, size_t nbytes);
  void alloc_cpu_am_handler(gasnet_token_t token, void *am, size_t nbytes);
  void alloc_gpu_am_handler(gasnet_token_t token, void *am, size_t nbytes);
  void alloc_reply_handler(gasnet_token_t token, void *reply, size_t nbytes);
  void free_cpu_am_handler(gasnet_token_t token, void *am, size_t nbytes);
  void free_gpu_am_handler(gasnet_token_t token, void *am, size_t nbytes);
  void inc_am_handler(gasnet_token_t token, void *am, size_t nbytes);

  MEDIUM_HANDLER_DECL(copy_and_signal_request, 4, 8);
  SHORT_HANDLER_DECL(copy_and_signal_reply, 2, 4);
  MEDIUM_HANDLER_DECL(am_request2, 3, 4);
  MEDIUM_HANDLER_DECL(am_request4, 5, 6);
  MEDIUM_HANDLER_DECL(am_request2p2i, 5, 8);
#ifdef CDENABLED
  SHORT_HANDLER_DECL(cd_gasnet_get_request, 4, 7);
  MEDIUM_HANDLER_DECL(cd_gasnet_get_reply, 2, 4);
  SHORT_HANDLER_DECL(cd_gasnet_get_reply_nack, 5, 8);
  SHORT_HANDLER_DECL(cd_gasnet_get_request_long, 4, 7);
  LONG_HANDLER_DECL(cd_gasnet_get_reply_long, 1, 2);
  SHORT_HANDLER_DECL(cd_gasnet_mark_done_get, 1, 2);
  SHORT_HANDLER_DECL(cd_gasnet_mark_done_put, 5, 8);
  MEDIUM_HANDLER_DECL(cd_gasnet_put_request, 3, 6);
  SHORT_HANDLER_DECL(cd_gasnet_put_reply, 5, 8);
  LONG_HANDLER_DECL(cd_gasnet_put_request_long, 3, 6);
  SHORT_HANDLER_DECL(cd_gasnet_put_request_short, 5, 9);
  SHORT_HANDLER_DECL(cd_gasnet_put_reply_nack, 4, 7);
  SHORT_HANDLER_DECL(cd_gasnet_put_reply_nack_long, 4, 7);
  SHORT_HANDLER_DECL(cd_release_blocked_thread_request, 0, 0);
  SHORT_HANDLER_DECL(cd_retrieve_log_request, 1, 2);
  MEDIUM_HANDLER_DECL(cd_retrieve_log_reply, 1, 2);
  SHORT_HANDLER_DECL(cd_retrieve_log_reply_long, 3, 5);
  SHORT_HANDLER_DECL(cd_gasnet_free_temp_buffer, 1, 2);
  //SHORT_HANDLER_DECL(cd_retrieve_log_request_long, 4, 7);
  //LONG_HANDLER_DECL(cd_retrieve_log_reply_long, 1, 2);
  //SHORT_HANDLER_DECL(cd_retrieve_log_reply_short, 3, 5);
#endif

  enum {
    gasneti_handleridx(copy_and_signal_request) = COPY_AND_SIGNAL_REQUEST, 
    gasneti_handleridx(copy_and_signal_reply) = COPY_AND_SIGNAL_REPLY, 
    gasneti_handleridx(am_request2) = GENERIC_AM_REQUEST2,
    gasneti_handleridx(am_request4) = GENERIC_AM_REQUEST4,
    gasneti_handleridx(am_request2p2i) = GENERIC_AM_REQUEST2P2I,
#ifdef CDENABLED
    gasneti_handleridx(cd_gasnet_get_request) = CD_GASNET_GET_REQUEST, 
    gasneti_handleridx(cd_gasnet_get_reply) = CD_GASNET_GET_REPLY, 
    gasneti_handleridx(cd_gasnet_get_reply_nack) = CD_GASNET_GET_REPLY_NACK, 
    gasneti_handleridx(cd_gasnet_get_request_long) = CD_GASNET_GET_REQUEST_LONG, 
    gasneti_handleridx(cd_gasnet_get_reply_long) = CD_GASNET_GET_REPLY_LONG, 
    gasneti_handleridx(cd_gasnet_mark_done_get) = CD_GASNET_MARK_DONE_GET, 
    gasneti_handleridx(cd_gasnet_mark_done_put) = CD_GASNET_MARK_DONE_PUT, 
    gasneti_handleridx(cd_gasnet_put_request) = CD_GASNET_PUT_REQUEST, 
    gasneti_handleridx(cd_gasnet_put_reply) = CD_GASNET_PUT_REPLY, 
    gasneti_handleridx(cd_gasnet_put_request_long) = CD_GASNET_PUT_REQUEST_LONG, 
    gasneti_handleridx(cd_gasnet_put_request_short) = CD_GASNET_PUT_REQUEST_SHORT, 
    gasneti_handleridx(cd_gasnet_put_reply_nack) = CD_GASNET_PUT_REPLY_NACK, 
    gasneti_handleridx(cd_gasnet_put_reply_nack_long) = CD_GASNET_PUT_REPLY_NACK_LONG, 
    gasneti_handleridx(cd_release_blocked_thread_request) = CD_RELEASE_BLOCKED_THREAD_REQUEST, 
    gasneti_handleridx(cd_retrieve_log_request) = CD_RETRIEVE_LOG_REQUEST, 
    gasneti_handleridx(cd_retrieve_log_reply) = CD_RETRIEVE_LOG_REPLY, 
    gasneti_handleridx(cd_retrieve_log_reply_long) = CD_RETRIEVE_LOG_REPLY_LONG, 
    gasneti_handleridx(cd_gasnet_free_temp_buffer) = CD_GASNET_FREE_TEMP_BUFFER, 
    //gasneti_handleridx(cd_retrieve_log_request_long) = CD_RETRIEVE_LOG_REQUEST_LONG, 
    //gasneti_handleridx(cd_retrieve_log_reply_long) = CD_RETRIEVE_LOG_REPLY_LONG, 
    //gasneti_handleridx(cd_retrieve_log_reply_short) = CD_RETRIEVE_LOG_REPLY_SHORT, 
#endif
  };
  
  //typedef struct {
  //  void *addr;
  //  uintptr_t size;
  //} gasnet_seginfo_t;

  // int gasnet_getSegmentInfo (gasnet_seginfo_t *seginfo table, int numentries);

  // This may need to be a thread-private data if GASNet supports per-thread segment in the future
  extern gasnet_seginfo_t *all_gasnet_seginfo;
  extern gasnet_seginfo_t *my_gasnet_seginfo;
  extern mspace _gasnet_mspace;
  extern gasnet_nodeinfo_t *all_gasnet_nodeinfo;
  extern gasnet_nodeinfo_t *my_gasnet_nodeinfo;
  extern gasnet_node_t my_gasnet_supernode;


  extern int env_use_am_for_copy_and_set; // defined in upcxx_runtime.cpp
  extern int env_use_dmapp; // defined in upcxx_runtime.cpp

  void gasnet_seg_free(void *p);
  void *gasnet_seg_memalign(size_t nbytes, size_t alignment);
  void *gasnet_seg_alloc(size_t nbytes);

  // Return the supernode of node n in GASNet
  static inline gasnet_node_t gasnet_supernode_of(gasnet_node_t n)
  {
    assert(all_gasnet_seginfo != NULL);
    return all_gasnet_nodeinfo[n].supernode;
  }

#if GASNET_PSHM
  static inline uintptr_t gasnet_pshm_offset(gasnet_node_t n)
  {
    assert(all_gasnet_nodeinfo != NULL);
    return all_gasnet_nodeinfo[n].offset;
  }
#endif
  
  void init_pshm_teams(const gasnet_nodeinfo_t *nodeinfo_from_gasnet,
                       uint32_t num_nodes);
} // namespace upcxx
