/**
 * team_for_cd.h - implements teams and collective operations for teams
 *
 */

#ifdef CDENABLED
#pragma once

#include <cassert>
#include <cstring>
#include <vector>
#include <iostream>

#include "upcxx_types.h"
#include "upcxx_runtime.h"
#include "gasnet_api.h"
#include "range.h"
#include "coll_flags.h"
#include "utils.h"
//#include "reduce.h"

#include "allocate_for_cd.h"

#ifdef CDENABLED
#define UPCXXR_WRAPPER_DECL(T, num_class, type_code)    \
  template<> struct datatype_wrapper<T> {               \
    typedef T number_type;                              \
    typedef T num_class;                                \
    static const upcxx_datatype_t value = type_code;    \
  }

#define UPCXXR_NUMBER_TYPE(T)                   \
  typename datatype_wrapper<T>::number_type
#define UPCXXR_INTEGER_TYPE(T)                  \
  typename datatype_wrapper<T>::int_type

#endif

/// \cond SHOW_INTERNAL

namespace upcxx
{
  struct team_for_cd;

  extern std::vector<team_for_cd *> *_team_stack_for_cd;

  template<class T> struct datatype_wrapper{};
  UPCXXR_WRAPPER_DECL(char, int_type, UPCXX_CHAR);
  UPCXXR_WRAPPER_DECL(unsigned char, int_type, UPCXX_UCHAR);
  UPCXXR_WRAPPER_DECL(short, int_type, UPCXX_SHORT);
  UPCXXR_WRAPPER_DECL(unsigned short, int_type, UPCXX_USHORT);
  UPCXXR_WRAPPER_DECL(int, int_type, UPCXX_INT);
  UPCXXR_WRAPPER_DECL(unsigned int, int_type, UPCXX_UINT);
  UPCXXR_WRAPPER_DECL(long, int_type, UPCXX_LONG);
  UPCXXR_WRAPPER_DECL(unsigned long, int_type, UPCXX_ULONG);
  UPCXXR_WRAPPER_DECL(long long, int_type, UPCXX_LONG_LONG);
  UPCXXR_WRAPPER_DECL(unsigned long long, int_type, UPCXX_ULONG_LONG);
  UPCXXR_WRAPPER_DECL(float, float_type, UPCXX_FLOAT);
  UPCXXR_WRAPPER_DECL(double, float_type, UPCXX_DOUBLE);

  //struct ts_scope;

  struct team_for_cd {
    team_for_cd() : _mychild(NULL) {
      if (_team_stack_for_cd != NULL && _team_stack_for_cd->size() > 0) {
        const team_for_cd *other = current_team();
        _parent = other->_parent;
        _size = other->_size;
        _myrank = other->_myrank;
        _color = other->_color;
        _team_id = other->_team_id;
        _gasnet_team = other->_gasnet_team;
      }
    }

    team_for_cd(const team_for_cd *other) : _mychild(NULL) {
      _parent = other->_parent;
      _size = other->_size;
      _myrank = other->_myrank;
      _color = other->_color;
      _team_id = other->_team_id;
      _gasnet_team = other->_gasnet_team;
    }
    
    team_for_cd(int flag) {
      assert(flag==-1); // only used to create an invalid team
      _mychild = NULL;
      _parent = NULL;
      _size = 0xFFFFFFFF;
      _myrank = 0xFFFFFFFF; 
      _color = 0xFFFFFFFF;
      _team_id = 0xFFFFFFFF;
      _gasnet_team = NULL;
    }
    
    ~team_for_cd()
    {
      // YZ: free the underlying gasnet team_for_cd?
      //      if (_gasnet_team != NULL) {
      //        gasnete_coll_team_free(_gasnet_team);
      //      }
    }

    inline uint32_t myrank() const { return _myrank; }
    
    inline uint32_t size() const { return _size; }
    
    inline uint32_t team_id() const { return _team_id; }
    
    inline team_for_cd *parent() const { return _parent; }

    inline team_for_cd *mychild() const { return _mychild; }

    inline uint32_t color() const { return _color; }

    inline gasnet_team_handle_t gasnet_team() const {
      return _gasnet_team;
    }

    inline bool is_team_all() const
    {
      return (_team_id == 0); // team_all has team_id 0
    }

    /**
     * Split a team_for_cd into new teams
     *
     * @param color processes with the same color will be in the same team_for_cd
     * @param relrank the new rank of the calling process in the new team_for_cd.  Note that it's different than the 'key' argument in MPI_Comm_split
     * @param new_team where the new team_for_cd pointer will be written for the caller to use the new team_for_cd
     * @return error value UPCXX_SUCCESS or UPCXX_ERROR
     */
    int split(uint32_t color, uint32_t relrank, team_for_cd *&new_team);

    int split(uint32_t color, uint32_t relrank);
    
    inline int barrier() const
    {
      int rv;
      assert(_gasnet_team != NULL);
      gasnet_coll_barrier_notify(_gasnet_team, 0,
                                 GASNET_BARRIERFLAG_ANONYMOUS);
      while ((rv=gasnet_coll_barrier_try(_gasnet_team, 0,
                                         GASNET_BARRIERFLAG_ANONYMOUS))
             == GASNET_ERR_NOT_READY) {
        if (advance_for_cd() < 0) { // process the async task queue
          return UPCXX_ERROR;
        }
      }
      assert(rv == GASNET_OK);
      return UPCXX_SUCCESS;

    }

    inline void barrier_notify() const 
    {
      assert(_gasnet_team != NULL);
      gasnet_coll_barrier_notify(_gasnet_team, 0,
                                 GASNET_BARRIERFLAG_ANONYMOUS);
    }

    inline int barrier_try() const
    {
      int rv;
      if ((rv=gasnet_coll_barrier_try(_gasnet_team, 0,
                                         GASNET_BARRIERFLAG_ANONYMOUS))
             == GASNET_ERR_NOT_READY) {
        if (advance_for_cd() < 0) { // process the async task queue
          //return UPCXX_ERROR;
          std::cout<<"Warning: should return UPCXX_ERROR here...\n";
        }
      }
      return rv;
    }

    inline int bcast(void *src, void *dst, size_t nbytes, uint32_t root) const
    {
      assert(_gasnet_team != NULL);
      gasnet_coll_broadcast(_gasnet_team, dst, root, src, nbytes,
                            UPCXX_GASNET_COLL_FLAG);
      return UPCXX_SUCCESS;
    }
  
    inline int gather(void *src, void *dst, size_t nbytes, uint32_t root) const
    {
      assert(_gasnet_team != NULL);
      gasnet_coll_gather(_gasnet_team, root, dst, src, nbytes, 
                         UPCXX_GASNET_COLL_FLAG);
      return UPCXX_SUCCESS;
    }

    inline int scatter(void *src, void *dst, size_t nbytes, uint32_t root) const
    {
      assert(_gasnet_team != NULL);
      gasnet_coll_scatter(_gasnet_team, dst, root, src, nbytes,
                          UPCXX_GASNET_COLL_FLAG);
      return UPCXX_SUCCESS;
    }

    inline int allgather(void *src, void *dst, size_t nbytes) const
    {
      assert(_gasnet_team != NULL);
      // YZ: gasnet_coll_gather_all is broken with Intel compiler on Linux!
      /*
      gasnet_coll_gather_all(_gasnet_team, dst, src, nbytes, 
                             UPCXX_GASNET_COLL_FLAG);
      */
      void *temp = allocate_for_cd(nbytes * size());
      assert(temp != NULL);
      gather(src, temp, nbytes, 0);
      bcast(temp, dst, nbytes * size(), 0);
      deallocate_for_cd(temp);
      return UPCXX_SUCCESS;
    }

    inline void alltoall(void *src, void *dst, size_t nbytes) const
    {
      gasnet_coll_exchange(_gasnet_team, dst, src, nbytes,
                           UPCXX_GASNET_COLL_FLAG);
    }

    template<class T>
    int reduce(T *src, T *dst, size_t count, uint32_t root,
               upcxx_op_t op) const
    {
      // We infer the data type from T by datatype_wrapper
      assert(_gasnet_team != NULL);
      gasnet_coll_reduce(_gasnet_team, root, dst, src, 0, 0, sizeof(T),
                         count, datatype_wrapper<T>::value, op,
                         UPCXX_GASNET_COLL_FLAG);
      return UPCXX_SUCCESS;
    }
    
    template<class T>
    int allreduce(T *src, T *dst, size_t count, upcxx_op_t op) const
    {
      // Reduce to 0 and then broadcast
      reduce(src, dst, count, 0, op);
      bcast(dst, dst, count*sizeof(T), 0);
      return UPCXX_SUCCESS;
    }

    /**
     * Translate a rank in a team_for_cd to its global rank
     */
    inline uint32_t team_rank_to_global(uint32_t team_rank) const
    {
      return gasnete_coll_team_rank2node(_gasnet_team, team_rank);
    }
    
    inline uint32_t global_rank_to_team(uint32_t global_rank) const
    {
      return gasnete_coll_team_node2rank(_gasnet_team, global_rank);
    }

    static uint32_t new_team_id();

    static team_for_cd *current_team() { return _team_stack_for_cd->back(); }

    static team_for_cd *global_team() { return (*_team_stack_for_cd)[0]; }

    friend int init(int *pargc, char ***pargv);

  private:
    team_for_cd *_parent, *_mychild;
    uint32_t _size, _myrank, _color;
    uint32_t _team_id;
    gasnet_team_handle_t _gasnet_team;

    team_for_cd(team_for_cd *parent, int size, int rank, int color,
         gasnet_team_handle_t handle) : _parent(parent),
      _mychild(NULL), _size(size), _myrank(rank), _color(color),
      _team_id(new_team_id()), _gasnet_team(handle) {}

    void team_for_cd_init(team_for_cd *parent, int size, int rank, int color, gasnet_team_handle_t handle) 
    {
      _parent = parent;
      _mychild = NULL;
      _size = size;
      _myrank = rank;
      _color = color;
      _team_id = new_team_id();
      _gasnet_team = handle;
    }

    void init_global_team() {
      _parent = NULL;
      _size = gasnet_nodes();
      _myrank = gasnet_mynode();
      _color = 0;
      _team_id = 0;
      _gasnet_team = GASNET_TEAM_ALL;
      // add to top of team_for_cd stack
      if (_team_stack_for_cd->size() == 0)
        _team_stack_for_cd->push_back(this);
    }

    static gasnet_hsl_t _tid_lock;
    // static std::vector<team_for_cd *> _team_stack_for_cd;

    //static void descend_team(team_for_cd *t) {
    //  if (t->_parent->_team_id != current_team()->_team_id) {
    //    std::cerr << "team_for_cd is not a subteam of current team_for_cd\n";
    //    abort();
    //  }
    //  _team_stack_for_cd->push_back(t);
    //}

    //static void ascend_team() {
    //  _team_stack_for_cd->pop_back();
    //}
  }; // end of struct team_for_cd
  
  inline
  std::ostream& operator<<(std::ostream& out, const team_for_cd& t)
  {
    return out << "team_for_cd: id " << t.team_id() << ", color " << t.color()
               << ", size " << t.size() << ", myrank " << t.myrank();
  }
  
  extern gasnet_team_handle_t current_gasnet_team_for_cd();

  static inline uint32_t ranks_for_cd() {
    if (is_init() == false) return 0xFFFFFFFF; // return a special value if not inited
    return team_for_cd::current_team()->size();
  }

  static inline uint32_t myrank_for_cd() {
    if (is_init() == false) return 0xFFFFFFFF; // return a special value if not inited
    return team_for_cd::current_team()->myrank();
  }

  extern team_for_cd team_all_for_cd;

} // namespace upcxx

#endif
