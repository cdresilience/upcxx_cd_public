/*
 * active_message.cpp - implement a wrapper of GASNet active messages for CD runtime
 */

#ifdef CDENABLED

#include <iostream>

#include "upcxx.h"
#include "upcxx/gasnet_api.h"
#include "upcxx/upcxx_internal.h"

namespace upcxx
{
  /*************************************************/
  /****** Long AM handlers for log retrieving ******/
  /*************************************************/
  //GASNETT_INLINE(cd_retrieve_log_reply_short_inner)
  //void cd_retrieve_log_reply_short_inner(gasnet_token_t token, void* addr, size_t nbytes, void* done){
  //  LOG_DEBUG("inside cd_retrieve_log_reply_short_inner with %ld bytes data\n", nbytes);
  //  char * buffer = (char*) addr;
  //  size_t* done_flag = (size_t*) done;

  //  gasnet_node_t dst_rank;
  //  gasnet_AMGetMsgSource(token, &dst_rank);

  //  CDHandle * cdh = GetCurrentCD();
  //  assert(cdh!=NULL);

  //  //unpack write logs and combine write log data with their corresponding SC/IWC counters
  //  cdh->UnpackWriteLogs(buffer, nbytes, dst_rank);
  //  
  //  (*done_flag)++;

  //  // free allocated gasnet segments
  //  gasnet_seg_free(addr);
  //  return;
  //}
  //SHORT_HANDLER(cd_retrieve_log_reply_short, 3, 5,
  //             (token, UNPACK(a0)      a1, UNPACK(a2)),
  //             (token, UNPACK2(a0,a1), a2, UNPACK2(a3,a4)));
  
  
  // long reply handler to retrieve logs
  //GASNETT_INLINE(cd_retrieve_log_reply_long_inner)
  //void cd_retrieve_log_reply_long_inner(gasnet_token_t token, void* addr, size_t nbytes, void* done){
  //  LOG_DEBUG("inside cd_retrieve_log_reply_long_inner with %ld bytes data\n", nbytes);
  //  char * buffer = (char*) addr;
  //  size_t* done_flag = (size_t*) done;

  //  gasnet_node_t dst_rank;
  //  gasnet_AMGetMsgSource(token, &dst_rank);

  //  CDHandle * cdh = GetCurrentCD();
  //  assert(cdh!=NULL);

  //  //unpack write logs and combine write log data with their corresponding SC/IWC counters
  //  cdh->UnpackWriteLogs(buffer, nbytes, dst_rank);
  //  
  //  (*done_flag)++;

  //  // free allocated gasnet segments
  //  gasnet_seg_free(addr);
  //  return;
  //}
  //LONG_HANDLER(cd_retrieve_log_reply_long, 1, 2,
  //            (token, addr, nbytes, UNPACK(a0)),
  //            (token, addr, nbytes, UNPACK2(a0,a1)));
  

  //// long request handler to retrieve logs
  //GASNETT_INLINE(cd_retrieve_log_request_long_inner)
  //void cd_retrieve_log_request_long_inner(gasnet_token_t token, size_t nbytes, void* done, void* src_addr, void* dest_addr){
  //  LOG_DEBUG("inside cd_retrieve_log_request_long_inner\n");

  //  if (nbytes>max_am_reply_size_long()){
  //    gasnet_node_t dst_rank;
  //    gasnet_AMGetMsgSource(token, &dst_rank);

  //    LOG_DEBUG("\x1b[31m payload size (%d B) exceeds AM_LONG_MAX(%d B), record address and length to use RDMA in advance\n\x1b[0m", 
  //            nbytes, max_am_reply_size_long());
  //    //gasnet_put_bulk(dst_rank, dest_addr, src_addr, nbytes);
  //    //GASNET_CHECK_RV(SHORT_REP(3,5,(token, CD_RETRIEVE_LOG_REPLY_SHORT, PACK(dest_addr), nbytes, PACK(done))));
  //    cd::rdma_op_t tmp_rdma_op(dst_rank, src_addr, nbytes, dest_addr, 0/*isget*/, done);
  //    cd::rdma_ops_for_cd_.push_back(tmp_rdma_op);
  //  }
  //  else {
  //    printf("cd_retrieve_log_request_long_inner::long_rep to send %dB write logs (limit::%d)\n", nbytes, max_am_reply_size_long());
  //    GASNET_CHECK_RV(LONG_REP(1,2,(token, CD_RETRIEVE_LOG_REPLY_LONG, src_addr, nbytes, dest_addr, PACK(done))));
  //  }

  //  //FIXME: can src_addr be deallocated here??
  //  free(src_addr);
  //  return;
  //}
  //SHORT_HANDLER(cd_retrieve_log_request_long, 4, 7,
  //             (token, a0, UNPACK(a1), UNPACK(a2), UNPACK(a3)),
  //             (token, a0, UNPACK2(a1,a2), UNPACK2(a3,a4), UNPACK2(a5,a6)));
  
  // reply handler to handle case where (write_log_size > gasnet_AMMaxMedium())
  GASNETT_INLINE(cd_retrieve_log_reply_long_inner)
  void cd_retrieve_log_reply_long_inner(gasnet_token_t token, size_t nbytes, void* done, void* src_addr){
    LOG_DEBUG("inside cd_retrieve_log_reply_long_inner \n");
    gasnet_node_t dst_rank;
    gasnet_AMGetMsgSource(token, &dst_rank);

    // allocate memory space for log transfer
    void* dest = (void*) gasnet_seg_alloc(nbytes);

    cd::rdma_op_t tmp_rdma_op(dst_rank, src_addr, nbytes, dest, 1/*isget*/, done);
    LOG_DEBUG("\x1b[31m rdma_op::thread_id=%d,src=%p,nbytes=%d,dest=%p,done=%p\n\x1b[0m",
            tmp_rdma_op.thread_id, tmp_rdma_op.src, tmp_rdma_op.nbytes, tmp_rdma_op.dest, tmp_rdma_op.done);
    cd::rdma_ops_for_cd_.push_back(tmp_rdma_op);

    return;
  }
  SHORT_HANDLER(cd_retrieve_log_reply_long, 3, 5,
               (token, a0, UNPACK(a1), UNPACK(a2)),
               (token, a0, UNPACK2(a1,a2), UNPACK2(a3,a4)));
  
  
  /***************************************************/
  /****** Medium AM handlers for log retrieving ******/
  /***************************************************/
  
  // reply handler to retrieve logs
  GASNETT_INLINE(cd_retrieve_log_reply_inner)
  void cd_retrieve_log_reply_inner(gasnet_token_t token, void* addr, size_t nbytes, void* done){
    LOG_DEBUG("inside cd_retrieve_log_reply_inner with %ld bytes data\n", nbytes);
    char * buffer = (char*) addr;
    size_t* done_flag = (size_t*) done;

    gasnet_node_t dst_rank;
    gasnet_AMGetMsgSource(token, &dst_rank);

    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    //unpack write logs and combine write log data with their corresponding SC/IWC counters
    cdh->UnpackWriteLogs(buffer, nbytes, dst_rank);
    
    (*done_flag)++;
    return;
  }
  MEDIUM_HANDLER(cd_retrieve_log_reply, 1, 2,
                (token, addr, nbytes, UNPACK(a0)),
                (token, addr, nbytes, UNPACK2(a0,a1)));
  
  
  // request handler to retrieve logs
  GASNETT_INLINE(cd_retrieve_log_request_inner)
  void cd_retrieve_log_request_inner(gasnet_token_t token, void* done){
    LOG_DEBUG("inside cd_retrieve_log_request_inner\n");

    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    //FIXME: memory allocation inside a request handler
    char* buffer;
    size_t nbytes;
    cdh->PackWriteLogs(src_rank, &buffer, &nbytes);
    LOG_DEBUG("After packwritelogs: buffer=%p, nbytes=%d\n", buffer, nbytes);

    if (nbytes <= max_am_payload_size()){
      GASNET_CHECK_RV(MEDIUM_REP(1,2,(token, CD_RETRIEVE_LOG_REPLY, buffer, nbytes, PACK(done))));
      gasnet_seg_free(buffer);
    }
    else {
      GASNET_CHECK_RV(SHORT_REP(3,5,(token, CD_RETRIEVE_LOG_REPLY_LONG, nbytes, PACK(done), PACK(buffer))));
    }

    return;
  }
  SHORT_HANDLER(cd_retrieve_log_request, 1, 2, 
               (token, UNPACK(a0)), 
               (token, UNPACK2(a0,a1)));

  void am_log_request(rank_t dst_rank, size_t* done){
    UPCXX_CALL_GASNET(
        GASNET_CHECK_RV(SHORT_REQ(1, 2, (dst_rank, CD_RETRIEVE_LOG_REQUEST, PACK(done)))));
  }

  void am_log_request_sync(size_t num, size_t *done){
    while(*done != num){
      advance();
    };
  }


  /****************************************************/
  /****** AM handlers for blocked threads release******/
  /****************************************************/
  
  // request handler to release a blocked thread
  extern uint32_t blocked;
  GASNETT_INLINE(cd_release_blocked_thread_request_inner)
  void cd_release_blocked_thread_request_inner(gasnet_token_t token){
    LOG_DEBUG("inside cd_release_blocked_thread_request_inner\n");

    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    // added into released_threads_ list, and the list will be examined in advance()
    cd::released_threads_.insert(src_rank);

    return;
  }
  SHORT_HANDLER(cd_release_blocked_thread_request, 0, 0, (token), (token));

  void am_release_blocked_threads(size_t dst_rank){
    // need to deal with all pending incoming writes before release blocked threads
    advance();
    UPCXX_CALL_GASNET(
        GASNET_CHECK_RV(SHORT_REQ(0, 0, (dst_rank, CD_RELEASE_BLOCKED_THREAD_REQUEST))));
  }

}

#endif
