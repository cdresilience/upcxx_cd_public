/**
 * team.cpp
 */

#include "upcxx.h"
#include "upcxx/upcxx_internal.h"

#ifdef CDENABLED
#include "upcxx/team_for_cd.h"
#endif

using namespace std;

namespace upcxx
{
  team team_all;
#ifdef CDENABLED
  team_for_cd team_all_for_cd;
  vector<team_for_cd *> * _team_stack_for_cd = NULL;
  gasnet_hsl_t team_for_cd::_tid_lock = GASNET_HSL_INITIALIZER;
#endif

  uint32_t my_team_seq = 1;
  gasnet_hsl_t team::_tid_lock = GASNET_HSL_INITIALIZER;
  vector<team *> * _team_stack = NULL;

  bool _threads_deprecated_warned = false;

  gasnet_team_handle_t current_gasnet_team()
  {
    return team::current_team()->gasnet_team();
  }

#ifdef CDENABLED
  gasnet_team_handle_t current_gasnet_team_for_cd()
  {
    return team_for_cd::current_team()->gasnet_team();
  }
#endif

#define TEAM_ID_BITS 8
#define TEAM_ID_SEQ_MASK 0xFF

  int team::split(uint32_t color,
                  uint32_t relrank,
                  team *&new_team)
  {
    // Performs the team split operation
    gasnet_seginfo_t scratch_seg;
    scratch_seg.size = gasnett_getenv_int_withdefault("GASNET_COLL_SCRATCH_SIZE",
                                                      2048*1024,1);
#ifdef USE_GASNET_FAST_SEGMENT
    scratch_seg.addr = gasnet_seg_alloc(scratch_seg.size);
#else
    scratch_seg.addr = malloc(scratch_seg.size);
#endif
    assert(scratch_seg.addr != NULL);


    gasnet_team_handle_t new_gasnet_team
      = gasnete_coll_team_split(_gasnet_team, color, relrank, &scratch_seg GASNETE_THREAD_GET);
    assert(new_gasnet_team != NULL);

    uint32_t team_sz = gasnet_coll_team_size(new_gasnet_team);
    // range r_tmp = range(0,0,0);
    new_team = new team(this, team_sz, relrank, color, new_gasnet_team);

    assert(new_team != NULL);

    if (_mychild == NULL) _mychild = new_team;

    return UPCXX_SUCCESS;
  } // team::split

  int team::split(uint32_t color,
                  uint32_t key)
  {
    if (_mychild) {
      std::cerr << "team has already been split; "
                << "split a copy instead" << endl;
      abort();
    }
    team *new_team;
    return split(color, key, new_team);
  }

  uint32_t team::new_team_id()
  {
    gasnet_hsl_lock(&team::_tid_lock);
    assert(my_team_seq < TEAM_ID_SEQ_MASK);
    uint32_t new_tid =
      (global_myrank() << TEAM_ID_BITS) + my_team_seq++;
    gasnet_hsl_unlock(&team::_tid_lock);
    return new_tid;
  }

  //long tot_vol_cd_split = 0;
#ifdef CDENABLED
  int team_for_cd::split(uint32_t color,
                         uint32_t relrank,
                         team_for_cd *&new_team)
  {
    // Performs the team_for_cd split operation
    gasnet_seginfo_t scratch_seg;
    scratch_seg.size = gasnett_getenv_int_withdefault("GASNET_COLL_SCRATCH_SIZE",
                                                      2048*1024,1);
    //tot_vol_cd_split+=scratch_seg.size;
#ifdef USE_GASNET_FAST_SEGMENT
    scratch_seg.addr = gasnet_seg_alloc(scratch_seg.size);
#else
    scratch_seg.addr = malloc(scratch_seg.size);
#endif
    assert(scratch_seg.addr != NULL);


    gasnet_team_handle_t new_gasnet_team
      = gasnete_coll_team_split(_gasnet_team, color, relrank, &scratch_seg GASNETE_THREAD_GET);
    assert(new_gasnet_team != NULL);

    uint32_t team_sz = gasnet_coll_team_size(new_gasnet_team);
    // range r_tmp = range(0,0,0);

    //SZ: CD runtime does not need to new a team..
    //new_team = new team_for_cd(this, team_sz, relrank, color, new_gasnet_team);
    new_team->team_for_cd_init(this, team_sz, relrank, color, new_gasnet_team);

    assert(new_team != NULL);

    if (_mychild == NULL) _mychild = new_team;

    return UPCXX_SUCCESS;
  } // team_for_cd::split

  int team_for_cd::split(uint32_t color,
                  uint32_t key)
  {
    if (_mychild) {
      std::cerr << "team_for_cd has already been split; "
                << "split a copy instead" << endl;
      abort();
    }
    team_for_cd *new_team;
    return split(color, key, new_team);
  }

  uint32_t team_for_cd::new_team_id()
  {
    gasnet_hsl_lock(&team_for_cd::_tid_lock);
    assert(my_team_seq < TEAM_ID_SEQ_MASK);
    uint32_t new_tid =
      (global_myrank() << TEAM_ID_BITS) + my_team_seq++;
    gasnet_hsl_unlock(&team_for_cd::_tid_lock);
    return new_tid;
  }

#endif

  std::vector< std::vector<rank_t> > *pshm_teams = NULL;

  void init_pshm_teams(const gasnet_nodeinfo_t *nodeinfo_from_gasnet,
                       uint32_t num_nodes)
  {
    if (pshm_teams != NULL) return;

    pshm_teams = new std::vector< std::vector<rank_t> >;
    if (nodeinfo_from_gasnet != NULL) {
      // Figure out the total number of supernodes
      gasnet_node_t max_supernode=0;
      for (uint32_t i=0; i<num_nodes; i++) {
        if (max_supernode < nodeinfo_from_gasnet[i].supernode) {
          max_supernode = nodeinfo_from_gasnet[i].supernode;
        }
      }
      // printf("max_supernode %u\n", max_supernode);
      pshm_teams->resize(max_supernode+1);

      for (uint32_t i=0; i<num_nodes; i++) {
        (*pshm_teams)[nodeinfo_from_gasnet[i].supernode].push_back(i);
      }
    } else {
      pshm_teams->resize(global_ranks());
      for (uint32_t i=0; i<global_ranks(); i++) {
        (*pshm_teams)[i].push_back(i);
      }
    }
  }
} // namespace upcxx
