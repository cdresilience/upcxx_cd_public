#include "upcxx/upcxx.h"
#include "upcxx/upcxx_internal.h"

#ifdef CDENABLED
#include "upcxx/cd_wrapper.h"
#endif

namespace upcxx {
#ifdef CDENABLED
  int barrier()
  {
    int rv;
    UPCXX_CALL_GASNET(cd_gasnet_coll_barrier_notify(current_gasnet_team(), 0,
                                                 GASNET_BARRIERFLAG_UNNAMED));
    do {
      UPCXX_CALL_GASNET(rv=cd_gasnet_coll_barrier_try(current_gasnet_team(), 0,
                                                   GASNET_BARRIERFLAG_UNNAMED));
      if (rv == GASNET_ERR_NOT_READY) {
        if (advance() < 0) { // process the async task queue
          return UPCXX_ERROR;
        }
      }
    } while (rv != GASNET_OK);

    return UPCXX_SUCCESS;
  }
#else
  int barrier()
  {
    int rv;
    UPCXX_CALL_GASNET(gasnet_coll_barrier_notify(current_gasnet_team(), 0,
                                                 GASNET_BARRIERFLAG_UNNAMED));
    do {
      UPCXX_CALL_GASNET(rv=gasnet_coll_barrier_try(current_gasnet_team(), 0,
                                                   GASNET_BARRIERFLAG_UNNAMED));
      if (rv == GASNET_ERR_NOT_READY) {
        if (advance() < 0) { // process the async task queue
          return UPCXX_ERROR;
        }
      }
    } while (rv != GASNET_OK);

    return UPCXX_SUCCESS;
  }

#endif
} // namespace upcxx
