#include "upcxx.h"
#include "upcxx/upcxx_internal.h"

#ifdef CDENABLED
#include "cd.h"
#include "upcxx/allocate_for_cd.h"
#endif

namespace upcxx
{
  //long tot_volume_alloc_app=0;
#ifdef CDENABLED
  //long tot_volume_alloc_cd=0;
  // for_cd allocate/deallocate functions
  global_ptr_for_cd<void> allocate_for_cd(rank_t rank, size_t nbytes)
  {
    void *addr;

#ifdef UPCXX_HAVE_CXX11
    if (nbytes == 0) return global_ptr_for_cd<void>(nullptr, rank);
#else
    if (nbytes == 0) return global_ptr_for_cd<void>(NULL, rank);
#endif

    if (rank == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      //tot_volume_alloc_cd+=nbytes;
      addr = gasnet_seg_alloc(nbytes);
#else
      addr = malloc(nbytes);
#endif
    } else {
      event e;
      e.incref();
      alloc_am_t am = { nbytes, &addr, &e };
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(rank, ALLOC_CPU_AM, &am, sizeof(am)));
      e.wait();
    }
    global_ptr_for_cd<void> ptr(addr, rank);

#ifdef DEBUG
    fprintf(stderr, "allocated %llu bytes at %p on node %d\n", nbytes, addr, rank);
#endif

    return ptr;
  }

  void deallocate_for_cd(global_ptr_for_cd<void> ptr)
  {
    if (ptr.raw_ptr() == NULL) return;

    if (ptr.where() == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      gasnet_seg_free(ptr.raw_ptr());
#else
      free(ptr.raw_ptr());
#endif
    } else {
      free_am_t am;
      am.ptr = ptr.raw_ptr();
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(ptr.where(), FREE_CPU_AM, &am, sizeof(am)));
    }
  }

  void deallocate_for_cd(void *ptr)
  {
    if (ptr == NULL) return;

#ifdef USE_GASNET_FAST_SEGMENT
    gasnet_seg_free(ptr);
#else
    free(ptr);
#endif
  } 


#if CD_LIBC_LOG_ENABLED
  global_ptr<void> allocate_inner(rank_t rank, size_t nbytes)
  {
    void *addr;

#ifdef UPCXX_HAVE_CXX11
    if (nbytes == 0) return global_ptr<void>(nullptr, rank);
#else
    if (nbytes == 0) return global_ptr<void>(NULL, rank);
#endif

    if (rank == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      //tot_volume_alloc_app+=nbytes;
      addr = gasnet_seg_alloc(nbytes);
#else
      addr = malloc(nbytes);
#endif
    } else {
      event e;
      e.incref();
      alloc_am_t am = { nbytes, &addr, &e };
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(rank, ALLOC_CPU_AM, &am, sizeof(am)));
      e.wait();
    }
    global_ptr<void> ptr(addr, rank);

#ifdef DEBUG
    fprintf(stderr, "allocated %llu bytes at %p on node %d\n", nbytes, addr, rank);
#endif

    return ptr;
  }

  void deallocate_inner(global_ptr<void> ptr)
  {
    if (ptr.raw_ptr() == NULL) return;

    if (ptr.where() == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      gasnet_seg_free(ptr.raw_ptr());
#else
      free(ptr.raw_ptr());
#endif
    } else {
      free_am_t am;
      am.ptr = ptr.raw_ptr();
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(ptr.where(), FREE_CPU_AM, &am, sizeof(am)));
    }
  }

  void deallocate_inner(void *ptr)
  {
    if (ptr == NULL) return;

#ifdef USE_GASNET_FAST_SEGMENT
    gasnet_seg_free(ptr);
#else
    free(ptr);
#endif
  } 

  global_ptr<void> allocate(rank_t rank, size_t nbytes)
  {
    //SZ
    if (app_side == false)
    {
      return allocate_inner(rank, nbytes);
    }
  
    app_side = false;
    CDHandle * cur_cd_handle = GetCurrentCD();
    if (cur_cd_handle == NULL)
    {
      global_ptr<void> tmp;
      //LOG_DEBUG("Allocating space out of CD context...\n");
      //-------------------
      tmp = allocate_inner(rank, nbytes);
      //-------------------
  
      app_side = true;
      return tmp;
    }
  
    assert(cur_cd_handle->libc_log_ptr() != NULL);
  
    switch (cur_cd_handle->GetCommLogMode_libc())
    {
      case kGenerateLog:
        {
          //-------------------
          global_ptr<void> tmp = allocate_inner(rank, nbytes);
          //-------------------
  
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          //SZTODO: need to think about how to preserve global_ptr
          IncompleteLogEntry log_entry = cur_cd_handle->NewLogEntry(rank, tmp.raw_ptr(), nbytes, true, 
                                                            cur_cd_handle->level(), cur_cd_handle->cur_pos_mem_alloc_log());
          cur_cd_handle->IncMemAllocLogPos();
          cur_cd_handle->mem_alloc_log().push_back(log_entry);
          LOG_DEBUG("libc_log_ptr_: (%u) %p\t - EXECUTE MODE malloc(%ld) = %p @place %d, @level %i  how many mem alloc logged?%lu\n", 
                      rank, cur_cd_handle->libc_log_ptr(), nbytes, tmp.raw_ptr(), tmp.where(), cur_cd_handle->level(), 
                      cur_cd_handle->mem_alloc_log().size());
          app_side = true;
          return tmp;
        }
        break;
  
      case kReplayLog:
        {
          void * tmp_addr;
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          //SZTODO: why mem_alloc_log_'s size == 0? Should not unpack logs from parents in CD_Begin??
          if (cur_cd_handle->mem_alloc_log().size() == 0)
          {
            LOG_DEBUG("RE-EXECUTION MODE, but no entries in malloc log! => get log from parent @ level %u\n", cur_cd_handle->level());
            tmp_addr = cur_cd_handle->MemAllocSearch();
            cur_cd_handle->IncMemAllocLogReplayPos();
          }
          else
          {
            tmp_addr = (void*) cur_cd_handle->mem_alloc_log().at(cur_cd_handle->replay_pos_mem_alloc_log()).p_;
            // TODO: should optimize following line to combine it with accessing tmp_addr
            assert(rank == (cur_cd_handle->mem_alloc_log().at(cur_cd_handle->replay_pos_mem_alloc_log()).thread_));
  
            cur_cd_handle->IncMemAllocLogReplayPos();
            //if(cur_cd_handle->replay_pos_mem_alloc_log() == cur_cd_handle->mem_alloc_log().size()){
            //  //simply reset
            //  cur_cd_handle->ResetMemAllocLogReplayPos();
            //}
          }
          
          global_ptr<void> tmp(tmp_addr, rank);
          app_side = true;
          return tmp;
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd_handle->GetCDLoggingMode());
        break;
    }
  
    app_side = true;
    ERROR_MESSAGE("Should not come to this place, will return a NULL global_ptr...\n");
    return (global_ptr<void>)NULL;
  }
  
  void deallocate(global_ptr<void> ptr)
  {
    //SZ
    if (app_side == false)
    {
      deallocate_inner(ptr);
      return;
    }
  
    app_side = false;
    CDHandle * cur_cd_handle = GetCurrentCD();
    if (cur_cd_handle == NULL)
    {
      //LOG_DEBUG("Deallocating space out of CD context...\n");
      //-------------------
      deallocate_inner(ptr);
      //-------------------
      app_side = true;
      return;
    }
  
    assert(cur_cd_handle->libc_log_ptr() != NULL);
  
    //FIXME: should we call deallocate_inner somewhere here? or at complete time??
    LOG_DEBUG("libc_free: FREE is invoked %p of rank(%d)\n", ptr.raw_ptr(), ptr.where());
    std::vector<IncompleteLogEntry>::iterator it;
    for (it=cur_cd_handle->mem_alloc_log().begin(); it!=cur_cd_handle->mem_alloc_log().end(); it++)
    {
      //address matched!
      if(it->p_ == ptr.raw_ptr() && it->thread_ == ptr.where()) 
      { 
        LOG_DEBUG("libc_free: - complete? %p of rank(%d)\n", ptr.raw_ptr(), ptr.where());
        app_side = true;
        return;
      }
    }
    
    //search & parent's log
    bool found = cur_cd_handle->PushedMemLogSearch(ptr.raw_ptr(), cur_cd_handle);
    if(found) 
    {
      LOG_DEBUG("libc_free: found %p of rank (%d) from parent's log will NOT free it \n", ptr.raw_ptr(), ptr.where());
      app_side = true;
      return;
    }
  
    LOG_DEBUG("Deallocating space...\n");
    //-------------------
    deallocate_inner(ptr);
    //-------------------
  
    app_side = true;
    return;
  }

  void deallocate(void* ptr)
  {
    //SZ
    if (app_side == false)
    {
      deallocate_inner(ptr);
      return;
    }
  
    app_side = false;
    CDHandle * cur_cd_handle = GetCurrentCD();
    if (cur_cd_handle == NULL)
    {
      //LOG_DEBUG("Deallocating space out of CD context...\n");
      //-------------------
      deallocate_inner(ptr);
      //-------------------
      app_side = true;
      return;
    }
  
    assert(cur_cd_handle->libc_log_ptr() != NULL);
  
    //FIXME: should we call deallocate_inner somewhere here? or at complete time??
    LOG_DEBUG("libc_free: FREE is invoked %p\n", ptr);
    std::vector<IncompleteLogEntry>::iterator it;
    for (it=cur_cd_handle->mem_alloc_log().begin(); it!=cur_cd_handle->mem_alloc_log().end(); it++)
    {
      //address matched!
      if(it->p_ == ptr) 
      { 
        LOG_DEBUG("libc_free: - complete? %p\n", ptr);
        app_side = true;
        return;
      }
    }
    
    //search & parent's log
    bool found = cur_cd_handle->PushedMemLogSearch(ptr, cur_cd_handle);
    if(found) 
    {
      LOG_DEBUG("libc_free: found %p from parent's log will NOT free it \n", ptr);
      app_side = true;
      return;
    }
  
    LOG_DEBUG("Deallocating space...\n");
    //-------------------
    deallocate_inner(ptr);
    //-------------------
  
    app_side = true;
    return;
  }

#else // #if CD_LIBC_LOG_ENABLED

  global_ptr<void> allocate(rank_t rank, size_t nbytes)
  {
    void *addr;

#ifdef UPCXX_HAVE_CXX11
    if (nbytes == 0) return global_ptr<void>(nullptr, rank);
#else
    if (nbytes == 0) return global_ptr<void>(NULL, rank);
#endif

    if (rank == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      addr = gasnet_seg_alloc(nbytes);
#else
      addr = malloc(nbytes);
#endif
    } else {
      event e;
      e.incref();
      alloc_am_t am = { nbytes, &addr, &e };
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(rank, ALLOC_CPU_AM, &am, sizeof(am)));
      e.wait();
    }
    global_ptr<void> ptr(addr, rank);

#ifdef DEBUG
    fprintf(stderr, "allocated %llu bytes at %p on node %d\n", nbytes, addr, rank);
#endif

    return ptr;
  }

  void deallocate(global_ptr<void> ptr)
  {
    if (ptr.raw_ptr() == NULL) return;

    if (ptr.where() == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      gasnet_seg_free(ptr.raw_ptr());
#else
      free(ptr.raw_ptr());
#endif
    } else {
      free_am_t am;
      am.ptr = ptr.raw_ptr();
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(ptr.where(), FREE_CPU_AM, &am, sizeof(am)));
    }
  }

  void deallocate(void *ptr)
  {
    if (ptr == NULL) return;

#ifdef USE_GASNET_FAST_SEGMENT
    gasnet_seg_free(ptr);
#else
    free(ptr);
#endif
  } 

#endif // #if CD_LIBC_LOG_ENABLED


#else // #ifdef CDENABLED
  global_ptr<void> allocate(rank_t rank, size_t nbytes)
  {
    void *addr;

#ifdef UPCXX_HAVE_CXX11
    if (nbytes == 0) return global_ptr<void>(nullptr, rank);
#else
    if (nbytes == 0) return global_ptr<void>(NULL, rank);
#endif

    if (rank == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      addr = gasnet_seg_alloc(nbytes);
#else
      addr = malloc(nbytes);
#endif
    } else {
      event e;
      e.incref();
      alloc_am_t am = { nbytes, &addr, &e };
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(rank, ALLOC_CPU_AM, &am, sizeof(am)));
      e.wait();
    }
    global_ptr<void> ptr(addr, rank);

#ifdef DEBUG
    fprintf(stderr, "allocated %llu bytes at %p on node %d\n", nbytes, addr, rank);
#endif

    return ptr;
  }

  void deallocate(global_ptr<void> ptr)
  {
    if (ptr.raw_ptr() == NULL) return;

    if (ptr.where() == global_myrank()) {
#ifdef USE_GASNET_FAST_SEGMENT
      gasnet_seg_free(ptr.raw_ptr());
#else
      free(ptr.raw_ptr());
#endif
    } else {
      free_am_t am;
      am.ptr = ptr.raw_ptr();
      UPCXX_CALL_GASNET(gasnet_AMRequestMedium0(ptr.where(), FREE_CPU_AM, &am, sizeof(am)));
    }
  }

  void deallocate(void *ptr)
  {
    if (ptr == NULL) return;

#ifdef USE_GASNET_FAST_SEGMENT
    gasnet_seg_free(ptr);
#else
    free(ptr);
#endif
  } 

#endif // #ifdef CDENABLED

  void alloc_cpu_am_handler(gasnet_token_t token, void *buf, size_t nbytes)
  {
    assert(buf != NULL);
    assert(nbytes == sizeof(alloc_am_t));
    alloc_am_t *am = (alloc_am_t *)buf;

#ifdef UPCXX_DEBUG
    std::cerr << "Rank " << global_myrank() << " is inside alloc_cpu_am_handler.\n";
#endif

    alloc_reply_t reply;
    reply.ptr_addr = am->ptr_addr; // pass back the ptr_addr from the scr node
#ifdef USE_GASNET_FAST_SEGMENT
    reply.ptr = gasnet_seg_alloc(am->nbytes);
#else
    reply.ptr = malloc(am->nbytes);
#endif

#ifdef UPCXX_DEBUG
    assert(reply.ptr != NULL);
    std::cerr << "Rank " << global_myrank() << " allocated " << am->nbytes
              << " memory at " << reply.ptr << "\n";
#endif

    reply.cb_event = am->cb_event;
    GASNET_CHECK_RV(gasnet_AMReplyMedium0(token, ALLOC_REPLY, &reply, sizeof(reply)));
  }

  void alloc_reply_handler(gasnet_token_t token, void *buf, size_t nbytes)
  {
    // internal error checking
    assert(buf != NULL);
    assert(nbytes == sizeof(alloc_reply_t));
    // end of internal error checking

    alloc_reply_t *reply = (alloc_reply_t *)buf;

#ifdef UPCXX_DEBUG
    std::cerr << "Rank " << global_myrank() << " is in alloc_reply_handler. reply->ptr "
              << reply->ptr << "\n";
#endif

    *(reply->ptr_addr) = reply->ptr;
    reply->cb_event->decref();
  }

  void free_cpu_am_handler(gasnet_token_t token, void *buf, size_t nbytes)
  {
    assert(buf != NULL);
    assert(nbytes == sizeof(free_am_t));
    free_am_t *am = (free_am_t *)buf;
    if (am->ptr != NULL) {
#ifdef USE_GASNET_FAST_SEGMENT
      gasnet_seg_free(am->ptr);
#else
      free(am->ptr);
#endif
    }
  }
} // namespace upcxx
