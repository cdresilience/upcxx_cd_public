/**
 * UPCXX events
 */

#ifdef CDENABLED
#include <stdio.h>
#include <assert.h>
#include <vector>

#include "upcxx.h"

//#define UPCXX_DEBUG

using namespace std;

namespace upcxx
{
  int event_for_cd::_async_try()
  {
    // Acquired the lock
#ifdef UPCXX_DEBUG
    fprintf(stderr, "Rank %u is calling async_try with event_for_cd %p\n", myrank(), this);
#endif

    // check outstanding gasnet handles
    if (!_gasnet_handles.empty()) {
      // for (auto it = _h.begin(); it != _h.end())  // UPCXX_HAVE_CXX11
      for (std::vector<gasnet_handle_t>::iterator it = _gasnet_handles.begin();
           it != _gasnet_handles.end();) {
#ifdef UPCXX_DEBUG
        fprintf(stderr, "Rank %u event_for_cd %p is checking handle %p in async_try.\n",
                myrank(), this, *it);
#endif
        // It's very important Not to poll GASNet while in async_try otherwise deadlocks may happen!
        int rv;
        UPCXX_CALL_GASNET(rv = gasnet_try_syncnb_nopoll(*it));
        if (rv == GASNET_OK) {
          LOG_DEBUG("Delete gasnet_handle (%p) for event_for_cd %p\n", *it, this);
          _gasnet_handles.erase(it); // erase increase it automatically
          _decref(1);
        } else {
          ++it;
        }
      }
    }

#ifdef UPCXX_USE_DMAPP
    // check outstanding dmapp handles
    if (!_dmapp_handles.empty()) {
     // for (auto it = _h.begin(); it != _h.end())  // UPCXX_HAVE_CXX11
     for (std::vector<dmapp_syncid_handle_t>::iterator it = _dmapp_handles.begin();
           it != _dmapp_handles.end();) {
       int flag;
       DMAPP_SAFE(dmapp_syncid_test(*it, &flag);
       if (flag) {
          _dmapp_handles.erase(it); // erase increase it automatically
          _decref(1);
        } else {
          ++it;
        }
      }
    }
#endif

    return isdone();
  }

  void event_for_cd::wait()
  {
    while (!async_try()) {
      advance_for_cd();
      // YZ: don't need to yield if CPUs are not over-subscribed.
      // gasnett_sched_yield();
    }
  }

  void event_for_cd::_enqueue_cb()
  {
    assert (_count == 0);

    // add done_cb to the task queue
    if (_num_done_cb > 0) {
      for (int i=0; i<_num_done_cb; i++) {
        if (_done_cb[i] != NULL) {
          async_task *task = _done_cb[i];
          if (task->_callee == global_myrank()) {
            // local task
            assert(in_task_queue != NULL);
            upcxx_mutex_lock(&in_task_queue_lock);
            queue_enqueue(in_task_queue, task);
            upcxx_mutex_unlock(&in_task_queue_lock);
          } else {
            // remote task
            assert(out_task_queue != NULL);
            upcxx_mutex_lock(&out_task_queue_lock);
            queue_enqueue(out_task_queue, task);
            upcxx_mutex_unlock(&out_task_queue_lock);
          }
        }
      }
      _num_done_cb = 0;
    }
  }

  int num_inc_system_event_for_cd = 0;
  int num_dec_system_event_for_cd = 0;

  // upcxx_mutex_t extra_event_lock = UPCXX_MUTEX_INITIALIZER;

  // Increment the reference counter for the event_for_cd
  void event_for_cd::_incref(uint32_t c)
  {
    if (c == 0) return;

    // upcxx_mutex_lock(&extra_event_lock);
    uint32_t old = _count;
    _count += c;
#ifdef UPCXX_DEBUG
    fprintf(stderr, "P %u _incref event_for_cd %p, c = %d, old %u\n", global_myrank(), this, c, old);
#endif

    // some sanity check
    if (this == system_event_for_cd) {
      num_inc_system_event_for_cd += c;
      if (num_inc_system_event_for_cd - num_dec_system_event_for_cd != _count) {
        fprintf(stderr, "Fatal error in _incref: Rank %u race condition happened for event_for_cd %p, _count %d, num_inc %d, num_dec %d.\n",
                myrank(), this, _count, num_inc_system_event_for_cd, num_dec_system_event_for_cd);
        gasnet_exit(1);
      }
    }

    if (this != system_event_for_cd && old == 0) {

#ifdef UPCXX_DEBUG
      fprintf(stderr, "P %u Add outstanding_event %p\n", global_myrank(), this);
#endif
      outstanding_events_for_cd->push_back(this);
    }
    // upcxx_mutex_unlock(&extra_event_lock);
  }

  // Decrement the reference counter for the event_for_cd
  void event_for_cd::_decref(uint32_t c)
  {
    if (c == 0) return;

#ifdef UPCXX_DEBUG
    fprintf(stderr, "P %u _decref event_for_cd %p, c = %d, old %u\n", global_myrank(), this, c, _count);
#endif
    // upcxx_mutex_lock(&extra_event_lock);

    _count -= c;

    if (this == system_event_for_cd) {
      num_dec_system_event_for_cd += c;
      if (num_inc_system_event_for_cd - num_dec_system_event_for_cd != _count) {
        fprintf(stderr, "Fatal error in _decref: Rank %u race condition happened for event_for_cd %p, _count %d, num_inc %d, num_dec %d.\n",
                myrank(), this, _count, num_inc_system_event_for_cd, num_dec_system_event_for_cd);
        gasnet_exit(1);
      }
    }

    if (_count < 0) {
      fprintf(stderr,
              "Fatal error: Rank %u attempt to decrement an event_for_cd (%p) to be a negative number of references!\n",
              global_myrank(), this);
      fprintf(stderr,
              "Fatal error: Rank %u this event_for_cd %p, _count %d, c %u, system_event_for_cd %p, num_inc %d, num_dec %d.\n",
              global_myrank(), this, _count, c, system_event_for_cd, num_inc_system_event_for_cd, num_dec_system_event_for_cd);
      gasnet_exit(1);
    }
    if (_count == 0  && this != system_event_for_cd) {
      _enqueue_cb();
#ifdef UPCXX_DEBUG
        fprintf(stderr, "P %u Erase outstanding_event %p\n", global_myrank(), this);
#endif
        outstanding_events_for_cd->remove(this);
    }
    // upcxx_mutex_unlock(&extra_event_lock);
  }

  //event_stack *events = NULL;

  void event_for_cd::_add_gasnet_handle(gasnet_handle_t h)
  {
#ifdef UPCXX_DEBUG
        fprintf(stderr, "Rank %u adds handles %p to event_for_cd %p\n",
                myrank(), h, this);
#endif
    _gasnet_handles.push_back(h);
    _incref();
  }

#ifdef UPCXX_USE_DMAPP
  void event_for_cd::add_dmapp_handle(dmapp_syncid_handle_t h)
  {
    _dmapp_handles.push_back(h);
    _incref();
  }

#endif

  void event_for_cd::_add_done_cb(async_task *task)
  {
    assert(_num_done_cb < MAX_NUM_DONE_CB);
    assert(this != system_event_for_cd);
    _done_cb[_num_done_cb] = task;
    _num_done_cb++;
  }

  //upcxx_mutex_t events_stack_lock = UPCXX_MUTEX_INITIALIZER;

  //void push_event(event_for_cd *e)
  //{
  //  upcxx_mutex_lock(&events_stack_lock);
  //  events->stack.push_back(e);
  //  upcxx_mutex_unlock(&events_stack_lock);
  //}

  //void pop_event()
  //{
  //  upcxx_mutex_lock(&events_stack_lock);
  //  assert(events->stack.back() != system_event_for_cd);
  //  events->stack.pop_back();
  //  upcxx_mutex_unlock(&events_stack_lock);
  //}

  //event_for_cd *peek_event()
  //{
  //  return events->stack.back();
  //}

  void async_wait_for_cd()
  {
    while (!outstanding_events_for_cd->empty()) {
      upcxx::advance_for_cd(10,10);
    }
    system_event_for_cd->wait();
    UPCXX_CALL_GASNET(gasnet_wait_syncnbi_all());
  }

  int async_try(event_for_cd *e)
  {
    // There is at least the default event_for_cd.
    assert (e != NULL);

    int rv = e->async_try();

    if (e == system_event_for_cd) {
      int rv1;
      UPCXX_CALL_GASNET(rv1 = gasnet_try_syncnbi_all());
      rv = rv && rv1;
    }
    return rv;
  }

  //void event_incref(event_for_cd *e, uint32_t c)
  //{
  //  assert(e != NULL);
  //  e->incref(c);
  //}
   
  //void event_decref(event_for_cd *e, uint32_t c)
  //{
  //  assert(e != NULL);
  //  e->decref(c);
  //}

} // namespace upcxx

#endif
