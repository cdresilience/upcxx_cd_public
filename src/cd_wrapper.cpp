//SZ
#ifdef CDENABLED
#include "cd.h"
#include "upcxx/cd_wrapper.h"
#include <gasnet_handler.h>
#include "upcxx/gasnet_api.h"
#include "upcxx/active_message.h"
#include "upcxx/upcxx_internal.h"

/* for definition of myrank*/
#include "upcxx/team.h"
#include "upcxx/lock.h"
#include "upcxx/allocate.h"

//#define INVALID_COUNTER (uint32_t)(-1)
#define SUNPACK(a0) ((size_t)UNPACK(a0))
#define SUNPACK2(a0, a1) ((size_t)UNPACK2(a0, a1))

namespace upcxx{

  extern uint32_t blocked;
  extern std::vector<void*> cd_outstanding_nbi;

  // reply handler for gasnet_get
  GASNETT_INLINE(cd_gasnet_get_reply_inner)
  void cd_gasnet_get_reply_inner(gasnet_token_t token, void* src, size_t nbytes, void* dest, void* done){
    LOG_DEBUG("\x1b[34m inside cd_gasnet_GET_reply_inner\x1b[0m\n");

    // copy data.
    memcpy(dest, src, nbytes);

    size_t * done_l = (size_t*)done;
    *done_l = 1;

    LOG_DEBUG("cd_gasnet_GET_reply_inner for %p (%d bytes) is DONE(&done=%p)\n", src, nbytes,done_l);
    return;
  }
  MEDIUM_HANDLER(cd_gasnet_get_reply, 2, 4,
                (token, addr, nbytes, UNPACK(a0)     , UNPACK(a1)),
                (token, addr, nbytes, UNPACK2(a0, a1), UNPACK2(a2, a3)));

  // reply handler to wait till target finishing recovery
  GASNETT_INLINE(cd_gasnet_get_reply_nack_inner)
  void cd_gasnet_get_reply_nack_inner(gasnet_token_t token, size_t nbytes, size_t islongam, void* dest, void* src, void* done){
    CDHandle * cdh = GetCurrentCD();
    assert(cdh != NULL);
    gasnet_node_t tgt_rank;
    gasnet_AMGetMsgSource(token, &tgt_rank);
    LOG_DEBUG("inside cd_gasnet_GET_reply_nack_inner due to task %d\n", tgt_rank);
    
    // Record Nacked AM message, and increment block_counter_
    bool tgt_recorded = cdh->RecordNackedAM(tgt_rank, src, nbytes, dest, islongam, 1/*isget*/, done);

    //blocking src threads from progressing...
    if (!blocked || !tgt_recorded){
      blocked++;
      LOG_DEBUG("\x1b[35m cd_gasnet_GET_reply_nack::blocked increased to %d due to failed target#%d\x1b[0m\n", blocked, tgt_rank);
    }
    return;
  }
  SHORT_HANDLER(cd_gasnet_get_reply_nack, 5, 8, 
                (token, a0, a1, UNPACK(a2)    , UNPACK(a3)    , UNPACK(a4)), 
                (token, a0, a1, UNPACK2(a2,a3), UNPACK2(a4,a5), UNPACK2(a6,a7)));

  // request handler for gasnet_get
  GASNETT_INLINE(cd_gasnet_get_request_inner)
  void cd_gasnet_get_request_inner(gasnet_token_t token, void* src, size_t nbytes, void* dest, void* done){
    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    // get src_rank
    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    // NACKed back if target is in recovery
    if (cdh->IsInRecovery()){
      //issue NACKs back to src rank
      LOG_DEBUG("rank#%d is in recovery, NACK back to src(%d)..\n", myrank(), src_rank);
      cdh->RecordBlockedThread(src_rank);
      GASNET_CHECK_RV(SHORT_REP(5,8,(token, CD_GASNET_GET_REPLY_NACK, nbytes, 0, PACK(dest), PACK(src), PACK(done))));
      return;
    }

    LOG_DEBUG("\x1b[34m inside cd_gasnet_GET_request_inner, return to rank#%d\x1b[0m\n", src_rank);

    //issue AMs back to src rank
    LOG_DEBUG("medium_rep cd_gasnet_get_reply to src rank (%d)..\n", src_rank);
    GASNET_CHECK_RV(MEDIUM_REP(2,4,(token, CD_GASNET_GET_REPLY, 
                                    src, nbytes, PACK(dest), PACK(done))));

    return;
  }
  SHORT_HANDLER(cd_gasnet_get_request, 4, 7,
               (token, UNPACK(a0),    , a1, UNPACK(a2)    , UNPACK(a3)    ),
               (token, UNPACK2(a0, a1), a2, UNPACK2(a3,a4), UNPACK2(a5,a6)));
  
  void cd_gasnet_wait_for_done(size_t *done){
    while(!(*done)){
      advance();
    }
  }

  //NOTE: skip_waiting flag is used to skip gets' waiting and logging, because it is a re-issue
  void cd_gasnet_get(void *dest, gasnet_node_t node, void *src, size_t nbytes, int skip_waiting, size_t *done)
  {
    //SZ: if function called by CD runtime, should bypass logging framework 
    if (app_side == false)
    {
      gasnet_get(dest, node, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_get(dest, node, src, nbytes);
      app_side = local_appside;
      return;
    }
  
    if (node == myrank()) {
      LOG_DEBUG("gasnet_get to self..\n");
      gasnet_get(dest, node, src, nbytes);
      app_side = local_appside;
      return;
    }

    int alloc_for_done = 0;
    if (done==NULL){
      done = new size_t;
      alloc_for_done = 1;
    }
    *done = 0;

    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //gasnet_get(dest, node, src, nbytes);
        GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST,
                                          PACK(src), nbytes, PACK(dest), PACK(done))));
        // blocking threads till gets are back..
        if (!skip_waiting)
          cd_gasnet_wait_for_done(done);
        break;
  
      case kRelaxedCDGen:
        {
          //gasnet_get(dest, node, src, nbytes);
          GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST,
                                            PACK(src), nbytes, PACK(dest), PACK(done))));
          // blocking threads till gets are back..
          if (!skip_waiting){
            cd_gasnet_wait_for_done(done);
            LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
            cur_cd->LogData(dest, nbytes);
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, nbytes);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            //gasnet_get(dest, node, src, nbytes);
            GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST,
                                              PACK(src), nbytes, PACK(dest), PACK(done))));
            // blocking threads till gets are back..
            if (!skip_waiting){
              cd_gasnet_wait_for_done(done);
              cur_cd->LogData(dest, nbytes);
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    if (alloc_for_done)
      delete done;
    app_side = local_appside;
  }

  // reply handler for gasnet_get
  GASNETT_INLINE(cd_gasnet_get_reply_long_inner)
  void cd_gasnet_get_reply_long_inner(gasnet_token_t token, void* src, size_t nbytes, void* done){
    LOG_DEBUG("\x1b[34m inside cd_gasnet_GET_reply_LONG_inner\x1b[0m\n");

    size_t * done_l = (size_t*)done;
    *done_l = 1;

    LOG_DEBUG("cd_gasnet_GET_reply_long_inner for %p (%d bytes) is DONE(&done=%p)\n", src, nbytes,done_l);
    return;
  }
  LONG_HANDLER(cd_gasnet_get_reply_long, 1, 2,
              (token, addr, nbytes, UNPACK(a0)),
              (token, addr, nbytes, UNPACK2(a0, a1)));

  GASNETT_INLINE(cd_gasnet_mark_done_get_inner)
  void cd_gasnet_mark_done_get_inner(gasnet_token_t token, void* done){
    LOG_DEBUG("\x1b[34m inside cd_gasnet_mark_done_get_inner\x1b[0m\n");

    size_t * done_l = (size_t*)done;
    *done_l = 1;

    LOG_DEBUG("cd_gasnet_mark_done_get_inner for (done::%p ) is DONE\n", done_l);
    return;
  }
  SHORT_HANDLER(cd_gasnet_mark_done_get, 1, 2,
               (token, UNPACK(a0)),
               (token, UNPACK2(a0, a1)));

  //GASNETT_INLINE(cd_gasnet_get_reply_short_inner)
  //void cd_gasnet_get_reply_short_inner(gasnet_token_t token, void* src, size_t nbytes, void* done){
  //  LOG_DEBUG("\x1b[34m inside cd_gasnet_get_reply_short_inner\x1b[0m\n");

  //  size_t * done_l = (size_t*)done;
  //  *done_l = 1;

  //  LOG_DEBUG("cd_gasnet_get_reply_short_inner for %p (%d bytes) is DONE(&done=%p)\n", src, nbytes,done_l);
  //  return;
  //}
  //SHORT_HANDLER(cd_gasnet_get_reply_short, 3, 5,
  //             (token, UNPACK(a0),      a1, UNPACK(a2)),
  //             (token, UNPACK2(a0, a1), a2, UNPACK2(a3,a4)));

  // request handler for gasnet_get
  GASNETT_INLINE(cd_gasnet_get_request_long_inner)
  void cd_gasnet_get_request_long_inner(gasnet_token_t token, void* src, size_t nbytes, void* dest, void* done){
    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    // get src_rank
    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    // NACKed back if target is in recovery
    if (cdh->IsInRecovery()){
      //issue NACKs back to src rank
      LOG_DEBUG("rank#%d is in recovery, NACK back to src(%d)..\n", myrank(), src_rank);
      cdh->RecordBlockedThread(src_rank);
      GASNET_CHECK_RV(SHORT_REP(5,8,(token, CD_GASNET_GET_REPLY_NACK, nbytes, 1, PACK(dest), PACK(src), PACK(done))));
      return;
    }

    LOG_DEBUG("\x1b[34m inside cd_gasnet_GET_request_LONG_inner, return to rank#%d\x1b[0m\n", src_rank);

    //issue AMs back to src rank
    if (nbytes < max_am_payload_size()){
      LOG_DEBUG("medium_rep cd_gasnet_get_reply to src rank (%d) and size (%d)..\n", src_rank, nbytes);
      GASNET_CHECK_RV(MEDIUM_REP(2,4,(token, CD_GASNET_GET_REPLY, 
                                      src, nbytes, PACK(dest), PACK(done))));
    }
    else if (nbytes < max_am_reply_size_long()){
      LOG_DEBUG("long_rep cd_gasnet_get_reply_long to src rank (%d) and size (%d)..\n", src_rank, nbytes);
      GASNET_CHECK_RV(LONG_REP(1,2,(token, CD_GASNET_GET_REPLY_LONG, 
                                      src, nbytes, dest, PACK(done))));
    }
    else{
      LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for gets\n\x1b[0m", 
                  nbytes, max_am_reply_size_long());

      ////SZNOTE: record RDMA ops and issue them in upcxx::advance()
      cd::rdma_op_t tmp_rdma_op(src_rank, src, nbytes, dest, false/*isget*/, done);
      //printf("\x1b[31m rmda_put::src_rank=%d,src=%p,nbytes=%d,dest=%p,isget=%d,done=%p\n\x1b[0m",
      //          tmp_rdma_op.thread_id, tmp_rdma_op.src, tmp_rdma_op.nbytes, tmp_rdma_op.dest, tmp_rdma_op.isget, tmp_rdma_op.done);
      cd::rdma_ops_.push_back(tmp_rdma_op);
    }

    return;
  }
  SHORT_HANDLER(cd_gasnet_get_request_long, 4, 7,
               (token, UNPACK(a0),    , a1, UNPACK(a2)    , UNPACK(a3)    ),
               (token, UNPACK2(a0, a1), a2, UNPACK2(a3,a4), UNPACK2(a5,a6)));
  
  
  // cd_gasnet_get_bulk with skip_waiting is essentially a cd_gasnet_get_nb_bulk
  void cd_gasnet_get_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes, int skip_waiting, size_t *done)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_get_bulk(dest, node, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_get_bulk(dest, node, src, nbytes);
      app_side = local_appside;
      return;
    }

    if (node == myrank()) {
      WARNING_MESSAGE("gasnet_get_bulk to self..\n");
      gasnet_get_bulk(dest, node, src, nbytes);
      app_side = local_appside;
      return;
    }

    int alloc_for_done = 0;
    if (done==NULL){
      done = new size_t;
      alloc_for_done = 1;
    }
    *done = 0;
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //gasnet_get_bulk(dest, node, src, nbytes);
        GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                          PACK(src), nbytes, PACK(dest), PACK(done))));
        // blocking threads till gets are back..
        if (!skip_waiting)
          cd_gasnet_wait_for_done(done);
        break;
  
      case kRelaxedCDGen:
        {
          //gasnet_get_bulk(dest, node, src, nbytes);
          GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                            PACK(src), nbytes, PACK(dest), PACK(done))));
          if (!skip_waiting){
            cd_gasnet_wait_for_done(done);
            LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
            cur_cd->LogData(dest, nbytes);
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, nbytes);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            //gasnet_get_bulk(dest, node, src, nbytes);
            GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                              PACK(src), nbytes, PACK(dest), PACK(done))));
            if (!skip_waiting){
              cd_gasnet_wait_for_done(done);
              cur_cd->LogData(dest, nbytes);
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    if (alloc_for_done)
      delete done;
    app_side = local_appside;
  }
  

  gasnet_handle_t cd_gasnet_get_nb_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes, size_t *done)
  {
    //SZ
    gasnet_handle_t tmp_h;
    if (app_side == false)
    {
      //FIXME: why will this function be called in a app_side::false context????
      WARNING_MESSAGE("calling cd_gasnet_get_nb_bulk in app_side::false context..\n");
      tmp_h = gasnet_get_nb_bulk(dest, node, src, nbytes);
      return tmp_h;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      tmp_h = gasnet_get_nb_bulk(dest, node, src, nbytes);
      app_side = local_appside;
      return tmp_h;
    }

    // local get_nb; this is a weird usage case..
    if (node == myrank()) {
      WARNING_MESSAGE("gasnet_get_nb_bulk to self..\n");
      tmp_h = gasnet_get_nb_bulk(dest, node, src, nbytes);
      app_side = local_appside;
      return tmp_h;
    }

    // done should be always a NULL, because re-issue will use cd_gasnet_get_bulk with skip_waiting..
    assert(done == NULL);
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //tmp_h = gasnet_get_nb_bulk(dest, node, src, nbytes);
        done = new size_t;
        *done = 0;
        GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                          PACK(src), nbytes, PACK(dest), PACK(done))));
        break;
  
      case kRelaxedCDGen:
        {
          //tmp_h = gasnet_get_nb_bulk(dest, node, src, nbytes);
          done = new size_t;
          *done = 0;
          GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                            PACK(src), nbytes, PACK(dest), PACK(done))));
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogData(dest, nbytes, node, false/*completed*/, (void*)done, true/*isrecv*/);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, nbytes);
          if (ret == kCommLogCommLogModeFlip){
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            //tmp_h = gasnet_get_nb_bulk(dest, node, src, nbytes);
            done = new size_t;
            *done = 0;
            GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                              PACK(src), nbytes, PACK(dest), PACK(done))));
            cur_cd->LogData(dest, nbytes, node, false/*completed*/, (void*)done, true/*isrecv*/);
          }
          else if (ret == kCommLogError){
            WARNING_MESSAGE("Incomplete log entry for nb comm! Needs to escalate, not implemented yet...!\n");
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    //return tmp_h;
    return (gasnet_handle_t)done;
  }
  
  
  void cd_gasnet_get_nbi_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes, size_t* done)
  {
    if (app_side == false)
    {
      gasnet_get_nbi_bulk(dest, node, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_get_nbi_bulk(dest, node, src, nbytes);
      app_side = local_appside;
      return;
    }
  
    // local get_nb; this is a weird usage case..
    if (node == myrank()) {
      WARNING_MESSAGE("gasnet_get_nbi_bulk to self..\n");
      gasnet_get_nbi_bulk(dest, node, src, nbytes);
      app_side = local_appside;
      return;
    }

    assert(done==NULL);

    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //gasnet_get_nbi_bulk(dest, node, src, nbytes);
        done = new size_t;
        *done = 0;
        GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                          PACK(src), nbytes, PACK(dest), PACK(done))));
        // record outstanding NBI messages
        cd_outstanding_nbi.push_back((void*)done);
        break;
  
      case kRelaxedCDGen:
        {
          done = new size_t;
          *done = 0;
          GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                            PACK(src), nbytes, PACK(dest), PACK(done))));
          //gasnet_get_nbi_bulk(dest, node, src, nbytes);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");

          //SZNOTE: inside LogDataImplicit, it will mark this operation is not completed..
          cur_cd->LogDataImplicit(dest, nbytes, node);
          // record outstanding NBI messages
          cd_outstanding_nbi.push_back((void*)done);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadDataImplicit(dest, nbytes);
          if (ret == kCommLogCommLogModeFlip){
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            //gasnet_get_nbi_bulk(dest, node, src, nbytes);
            done = new size_t;
            *done = 0;
            GASNET_CHECK_RV(SHORT_REQ(3, 7, (node, CD_GASNET_GET_REQUEST_LONG,
                                              PACK(src), nbytes, PACK(dest), PACK(done))));
            cur_cd->LogDataImplicit(dest, nbytes, node);
            // record outstanding NBI messages
            cd_outstanding_nbi.push_back((void*)done);
          }
          else if (ret == kCommLogError){
            WARNING_MESSAGE("Incomplete log entry for nb comm! Needs to escalate, not implemented yet...!\n");
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return;
  }
  

  void cd_wait_nbi_status_all(){
    while (cd_check_nbi_status_all()!=GASNET_OK){
      advance();
    }
  }
  
  void cd_gasnet_wait_syncnbi_all()
  {
    if (app_side == false)
    {
      //gasnet_wait_syncnbi_all();
      //WARNING_MESSAGE("cd_gasnet_wait_syncnbi_all called with app_side::false..\n");
      cd_wait_nbi_status_all();
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_wait_syncnbi_all();
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //gasnet_wait_syncnbi_all();
        cd_wait_nbi_status_all();
        cur_cd->ProbeAndLogDataImplicit();
        break;
  
      case kRelaxedCDGen:
        {
          //gasnet_wait_syncnbi_all();
          cd_wait_nbi_status_all();
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->ProbeAndLogDataImplicit();
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ProbeData(NULL, 0);
          if (ret == kCommLogCommLogModeFlip){
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            LOG_DEBUG("Should not come here because error happens between nb get/put and wait/try...\n");
            //gasnet_wait_syncnbi_all();
            cd_wait_nbi_status_all();
            cur_cd->ProbeAndLogDataImplicit();
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return;
  }
  
  int cd_gasnet_try_syncnb_nopoll_log_event(event* pevent)
  {
    if (app_side == false) {
      return GASNET_OK;;
    }
    bool local_appside = app_side;
    app_side = false;

    CDHandle *cur_cd = GetCurrentCD();
    if (cur_cd==NULL) {
      app_side = local_appside;
      return GASNET_OK;
    }

    //CD_DEBUG("event::_async_try logs event with no gasnet_handlers, suspect in recovery..\n");

    int tmp_r = GASNET_OK;
    switch(cur_cd->GetCDLoggingMode()){
      case kStrictCD:
        break;

      case kRelaxedCDGen:
        if (!pevent->cd_needed_event_logs.empty() || pevent==system_event){
          cur_cd->LogData(&tmp_r, sizeof(int));
          pevent->cd_needed_event_logs.clear();
        }
        break;

      case kRelaxedCDRead:
        cur_cd->ReadData(&tmp_r, sizeof(int));
        break;

      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }

    app_side = local_appside;
    return tmp_r;
  }

  int cd_gasnet_try_syncnb_nopoll(gasnet_handle_t handle, int log_event, event* pevent)
  {
    if (app_side == false)
    {
      //return gasnet_try_syncnb_nopoll(handle);
      WARNING_MESSAGE("cd_gasnet_try_syncnb_nopoll called with app_side::false..\n");
      size_t* done = (size_t*) handle;
      if (*done) {
        //pevent->cd_needed_event_logs.push_back(handle);
        return GASNET_OK;
      }
      else 
        return GASNET_ERR_NOT_READY;
    }
  
    int tmp_r;
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      tmp_r = gasnet_try_syncnb_nopoll(handle);
      app_side = local_appside;
      return tmp_r;
    }

    size_t * done = (size_t*) handle;
    tmp_r = GASNET_ERR_NOT_READY;
  
    //LOG_DEBUG("reaching here with CDLoggingMode = %d\n", cur_cd->GetCDLoggingMode());
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //tmp_r = gasnet_try_syncnb_nopoll(handle);
        if (*done) {
          tmp_r = GASNET_OK;
          LOG_DEBUG("Operation complete in one strict CD, ProbeAndLogData..\n");
          cur_cd->ProbeAndLogData((void*)handle);
        }
        break;
  
      case kRelaxedCDGen:
        {
          //tmp_r = gasnet_try_syncnb_nopoll(handle);
          if (*done){
            tmp_r = GASNET_OK;
            LOG_DEBUG("Operation complete, log flag and data...\n");
            if (log_event){
              cur_cd->LogData(&tmp_r, sizeof(int));
            }
            else {
              pevent->cd_needed_event_logs.push_back(handle);
            }
            cur_cd->ProbeAndLogData((void*)handle);
          }
          else if (log_event){
            cur_cd->LogData(&tmp_r, sizeof(int), 0, true, (void*)handle, false, true);
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          //LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(&tmp_r, sizeof(int));
          if (ret == kCommLogOK){
            //if (tmp_r == GASNET_OK){
            //  cur_cd->ProbeData(&handle, 0);
            //}
          }
          else if (ret == kCommLogCommLogModeFlip){
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            LOG_DEBUG("Should not come here because error happens between nb get/put and wait/try...\n");
            //tmp_r = gasnet_try_syncnb_nopoll(handle);
            if (*done){
              tmp_r = GASNET_OK;
              LOG_DEBUG("Operation complete, log flag and data...\n");
              if (log_event){
                cur_cd->LogData(&tmp_r, sizeof(int));
              }
              //else {
                pevent->cd_needed_event_logs.push_back(handle);
              //}
              cur_cd->ProbeAndLogData((void*)handle);
            }
            else {
              if (log_event){
                cur_cd->LogData(&tmp_r, sizeof(int), 0, true, (void*)handle, false, true);
              }
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return tmp_r;
  }
  

  int cd_check_nbi_status_all(){
    size_t* done;
    std::vector<void*>::iterator ii=cd_outstanding_nbi.begin();
    for (; ii!=cd_outstanding_nbi.end(); ii++){
      done = (size_t*)(*ii);
      if (!(*done)){
        break;
      }
    }

    if (ii == cd_outstanding_nbi.end()){
      return GASNET_OK;
    }
    else {
      return GASNET_ERR_NOT_READY;
    }
  }
  
  int cd_gasnet_try_syncnbi_all()
  {
    if (app_side == false)
    {
      //return gasnet_try_syncnbi_all();
      //WARNING_MESSAGE("cd_gasnet_try_syncnbi_all called with app_side::false..\n");
      return cd_check_nbi_status_all();
    }
  
    int tmp_r;
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      tmp_r = gasnet_try_syncnbi_all();
      app_side = local_appside;
      return tmp_r;
    }

    tmp_r = GASNET_ERR_NOT_READY;
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //tmp_r = gasnet_try_syncnbi_all();
        tmp_r = cd_check_nbi_status_all();
        if (tmp_r == GASNET_OK) {
          cur_cd->ProbeAndLogDataImplicit();
          cd_outstanding_nbi.clear();
        }
        break;
  
      case kRelaxedCDGen:
        {
          //tmp_r = gasnet_try_syncnbi_all();
          tmp_r = cd_check_nbi_status_all();
          if (tmp_r == GASNET_OK) {
            LOG_DEBUG("Operation complete, log flag and data...\n");
            cur_cd->LogData(&tmp_r, sizeof(int));
            cur_cd->ProbeAndLogDataImplicit();
            cd_outstanding_nbi.clear();
          }
          else {
            cur_cd->LogData(&tmp_r, sizeof(int), 0, true, NULL, false, true);
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          //LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(&tmp_r, sizeof(int));
          if (ret == kCommLogOK){
            if (tmp_r == GASNET_OK){
              cur_cd->ProbeData(NULL, 0);
            }
          }
          else if (ret == kCommLogCommLogModeFlip){
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            LOG_DEBUG("Should not come here because error happens between nb get/put and wait/try...\n");
            //tmp_r = gasnet_try_syncnbi_all();
            tmp_r = cd_check_nbi_status_all();
            if (tmp_r == GASNET_OK) {
              LOG_DEBUG("Operation complete, log flag and data...\n");
              cur_cd->LogData(&tmp_r, sizeof(int));
              cur_cd->ProbeAndLogDataImplicit();
              cd_outstanding_nbi.clear();
            }
            else {
              cur_cd->LogData(&tmp_r, sizeof(int), 0, true, NULL, false, true);
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return tmp_r;
  }
  
  
  void cd_gasnet_coll_reduce(gasnet_team_handle_t team,
                             gasnet_image_t dstimage, void *dst,
                             void *src, size_t src_blksz, size_t src_offset,
                             size_t elem_size, size_t elem_count,
                             gasnet_coll_fn_handle_t func, int func_arg,
                             int flags)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_coll_reduce(team, dstimage, dst, src, src_blksz, src_offset,
                         elem_size, elem_count, func, func_arg, flags);
      return;
    }
    
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_coll_reduce(team, dstimage, dst, src, src_blksz, src_offset,
                         elem_size, elem_count, func, func_arg, flags);
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //LOG_DEBUG("For strict CDs, no logging related operations...\n");
        gasnet_coll_reduce(team, dstimage, dst, src, src_blksz, src_offset,
                           elem_size, elem_count, func, func_arg, flags);
        break;
  
      case kRelaxedCDGen:
        {
          gasnet_coll_reduce(team, dstimage, dst, src, src_blksz, src_offset,
                             elem_size, elem_count, func, func_arg, flags);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogData(dst, elem_size*elem_count);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dst, elem_size*elem_count);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            gasnet_coll_reduce(team, dstimage, dst, src, src_blksz, src_offset,
                               elem_size, elem_count, func, func_arg, flags);
            cur_cd->LogData(dst, elem_size*elem_count);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
  }
  
  void cd_gasnet_coll_broadcast(gasnet_team_handle_t team,
                                void *dst,
                                gasnet_image_t srcimage, void *src,
                                size_t nbytes, int flags)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_coll_broadcast(team, dst, srcimage, src, nbytes, flags);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_coll_broadcast(team, dst, srcimage, src, nbytes, flags);
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //LOG_DEBUG("For strict CDs, no logging related operations...\n");
        gasnet_coll_broadcast(team, dst, srcimage, src, nbytes, flags);
        break;
  
      case kRelaxedCDGen:
        {
          gasnet_coll_broadcast(team, dst, srcimage, src, nbytes, flags);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogData(dst, nbytes);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dst, nbytes);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            gasnet_coll_broadcast(team, dst, srcimage, src, nbytes, flags);
            cur_cd->LogData(dst, nbytes);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
  }
  
  
  void cd_gasnet_coll_gather(gasnet_team_handle_t team,
                             gasnet_image_t dstimage, void *dst,
                             void *src,
                             size_t nbytes, int flags)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_coll_gather(team, dstimage, dst, src, nbytes, flags);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_coll_gather(team, dstimage, dst, src, nbytes, flags);
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //LOG_DEBUG("For strict CDs, no logging related operations...\n");
        gasnet_coll_gather(team, dstimage, dst, src, nbytes, flags);
        break;
  
      case kRelaxedCDGen:
        {
          gasnet_coll_gather(team, dstimage, dst, src, nbytes, flags);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          if (myrank()== dstimage) {
            cur_cd->LogData(dst, nbytes);
          }
          else {
            cur_cd->LogData(src, 0);
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret;
          if (myrank()== dstimage) {
            ret = cur_cd->ReadData(dst, nbytes);
          }
          else {
            ret = cur_cd->ProbeData(src, 0);
          }
  
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            gasnet_coll_gather(team, dstimage, dst, src, nbytes, flags);
            if (myrank()== dstimage) {
              cur_cd->LogData(dst, nbytes);
            }
            else {
              cur_cd->LogData(src, 0);
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
  }
  
  
  void cd_gasnet_coll_gather_all(gasnet_team_handle_t team,
                             void *dst, void *src,
                             size_t nbytes, int flags)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_coll_gather_all(team, dst, src, nbytes, flags);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_coll_gather_all(team, dst, src, nbytes, flags);
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //LOG_DEBUG("For strict CDs, no logging related operations...\n");
        gasnet_coll_gather_all(team, dst, src, nbytes, flags);
        break;
  
      case kRelaxedCDGen:
        {
          gasnet_coll_gather_all(team, dst, src, nbytes, flags);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogData(dst, nbytes);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dst, nbytes);
  
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            gasnet_coll_gather_all(team, dst, src, nbytes, flags);
            cur_cd->LogData(dst, nbytes);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
  }
  
  
  void cd_gasnet_coll_exchange(gasnet_team_handle_t team,
                               void *dst, void *src,
                               size_t nbytes, int flags)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_coll_exchange(team, dst, src, nbytes, flags);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_coll_exchange(team, dst, src, nbytes, flags);
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //LOG_DEBUG("For strict CDs, no logging related operations...\n");
        gasnet_coll_exchange(team, dst, src, nbytes, flags);
        break;
  
      case kRelaxedCDGen:
        {
          gasnet_coll_exchange(team, dst, src, nbytes, flags);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogData(dst, nbytes);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(dst, nbytes);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            gasnet_coll_exchange(team, dst, src, nbytes, flags);
            cur_cd->LogData(dst, nbytes);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
  }
  
  void cd_gasnet_coll_barrier_notify(gasnet_team_handle_t team,
                                     int id,
                                     int flags)
  {
    //SZ
    if (app_side == false)
    {
      gasnet_coll_barrier_notify(team, id, flags);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_coll_barrier_notify(team, id, flags);
      app_side = local_appside;
      return;
    }
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //LOG_DEBUG("For strict CDs, no logging related operations...\n");
        gasnet_coll_barrier_notify(team, id, flags);
        cur_cd->IncSyncCounter();
        break;
  
      case kRelaxedCDGen:
        {
          gasnet_coll_barrier_notify(team, id, flags);
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogData(NULL, 0);
          cur_cd->IncSyncCounter();
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ProbeData(NULL, 0);
          cur_cd->IncSyncCounter(true/*isreplay*/);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            gasnet_coll_barrier_notify(team, id, flags);
            cur_cd->LogData(NULL, 0);
            cur_cd->IncSyncCounter();
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
  }
  
  int cd_gasnet_coll_barrier_try(gasnet_team_handle_t team,
                                 int id,
                                 int flags)
  {
    //SZ
    if (app_side == false)
    {
      return gasnet_coll_barrier_try(team, id, flags);
    }
  
    int tmp_r;
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      tmp_r = gasnet_coll_barrier_try(team, id, flags);
      app_side = local_appside;
      return tmp_r;
    }
  
    //LOG_DEBUG("reaching here with CDLoggingMode = %d\n", cur_cd->GetCDLoggingMode());
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        tmp_r = gasnet_coll_barrier_try(team, id, flags);
        if (tmp_r == GASNET_OK) {
          cur_cd->IncSyncCounter();
        }
        break;
  
      case kRelaxedCDGen:
        {
          tmp_r = gasnet_coll_barrier_try(team, id, flags);
          //LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          if (tmp_r == GASNET_OK) {
            LOG_DEBUG("Operation complete, log completed flag...\n");
            cur_cd->LogData(&tmp_r, sizeof(int));
            cur_cd->IncSyncCounter();
          }
          else {
            cur_cd->LogData(&tmp_r, sizeof(int), 0, true, 0, false, true);
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          //LOG_DEBUG("In kReplay mode, replaying from logs...\n");
          CommLogErrT ret = cur_cd->ReadData(&tmp_r, sizeof(int));
          if (ret == kCommLogOK && tmp_r == GASNET_OK){
            cur_cd->IncSyncCounter(true/*isreplay*/);
          }
          if (ret == kCommLogCommLogModeFlip){
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            LOG_DEBUG("Error happens between barrier notify and wait, not sure if it has other effects...\n");
            tmp_r = gasnet_coll_barrier_try(team, id, flags);
            if (tmp_r == GASNET_OK) {
              LOG_DEBUG("Operation complete, log completed flag...\n");
              cur_cd->LogData(&tmp_r, sizeof(int));
              cur_cd->IncSyncCounter();
            }
            else {
              cur_cd->LogData(&tmp_r, sizeof(int), 0, true, 0, false, true);
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }

    // handling outstanding events if there is any
    cur_cd->CheckMailBox();
  
    app_side = local_appside;
    return tmp_r;
  }

  /*****************************************/
  /****** AM handlers for remote puts ******/
  /*****************************************/
  
  extern uint32_t incoming_write_counter;
  
  // reply handler for gasnet_put
  GASNETT_INLINE(cd_gasnet_put_reply_inner)
  void cd_gasnet_put_reply_inner(gasnet_token_t token, size_t sc, size_t iwc, void* src/*flag*/, void* tgt_addr, void* done){
    LOG_DEBUG("\x1b[34m inside gasnet_PUT_reply_inner\x1b[0m\n");
    CDHandle * cdh = GetCurrentCD();
    assert(cdh != NULL);

    gasnet_node_t tgt_rank;
    gasnet_AMGetMsgSource(token, &tgt_rank);

    // skip logging if source has reached one strict CD
    if (cdh->GetCDLoggingMode()==kRelaxedCDGen || cdh->GetCDLoggingMode()==kRelaxedCDRead)
      cdh->LogWriteData(tgt_addr, 0, tgt_rank, sc, iwc, src);

    size_t* t_done = (size_t*) done;
    *t_done = 1;

    LOG_DEBUG("cd_gasnet_PUT_reply_inner for %p is DONE with sc %lu and iwc %lu\n", src, sc, iwc);
    return;
  }
  SHORT_HANDLER(cd_gasnet_put_reply, 5, 8,
                (token, a0, a1, UNPACK(a2),     UNPACK(a3),     UNPACK(a4)),
                (token, a0, a1, UNPACK2(a2,a3), UNPACK2(a4,a5), UNPACK2(a6,a7)));

  // reply handler for gasnet_put
  GASNETT_INLINE(cd_gasnet_mark_done_put_inner)
  void cd_gasnet_mark_done_put_inner(gasnet_token_t token, size_t sc, size_t iwc, void* src/*flag*/, void* tgt_addr, void* done){
    LOG_DEBUG("\x1b[34m inside gasnet_PUT_reply_inner\x1b[0m\n");
    CDHandle * cdh = GetCurrentCD();
    assert(cdh != NULL);

    gasnet_node_t tgt_rank;
    gasnet_AMGetMsgSource(token, &tgt_rank);

    // skip logging if source has reached one strict CD
    if (cdh->GetCDLoggingMode()==kRelaxedCDGen || cdh->GetCDLoggingMode()==kRelaxedCDRead)
      cdh->LogWriteData(tgt_addr, 0, tgt_rank, sc, iwc, src);

    size_t* t_done = (size_t*) done;
    *t_done = 1;

    LOG_DEBUG("cd_gasnet_mark_done_PUT_inner for %p is DONE with sc %lu and iwc %lu\n", src, sc, iwc);
    return;
  }
  SHORT_HANDLER(cd_gasnet_mark_done_put, 5, 8,
                (token, a0, a1, UNPACK(a2),     UNPACK(a3),     UNPACK(a4)),
                (token, a0, a1, UNPACK2(a2,a3), UNPACK2(a4,a5), UNPACK2(a6,a7)));

  GASNETT_INLINE(cd_gasnet_put_reply_nack_inner)
  void cd_gasnet_put_reply_nack_inner(gasnet_token_t token, size_t nbytes, void* dest, void* org_src, void* done){
    CDHandle * cdh = GetCurrentCD();
    assert(cdh != NULL);

    gasnet_node_t tgt_rank;
    gasnet_AMGetMsgSource(token, &tgt_rank);

    LOG_DEBUG("inside put_reply_nack_inner due to task %d\n", tgt_rank);
    
    // Record Nacked AM message, and increment block_counter_
    bool tgt_recorded = cdh->RecordNackedAM(tgt_rank, org_src, nbytes, dest, 0/*islongam*/, 0/*isget*/, done);

    //blocking src threads from progressing...
    if (!blocked || !tgt_recorded){
      blocked++;
      LOG_DEBUG("\x1b[34m gasnet_PUT_reply_nack::blocked increased to %d due to failed target#%d\x1b[0m\n", blocked, tgt_rank);
    }

    return;
  }
  SHORT_HANDLER(cd_gasnet_put_reply_nack, 4, 7, 
                (token, a0, UNPACK(a1),     UNPACK(a2),     UNPACK(a3)), 
                (token, a0, UNPACK2(a1,a2), UNPACK2(a3,a4), UNPACK2(a5,a6)));

  GASNETT_INLINE(cd_gasnet_put_request_short_inner)
  void cd_gasnet_put_request_short_inner(gasnet_token_t token, void* src, size_t nbytes, void* dest, void* org_src, void* done){
    uint32_t old_iwc = incoming_write_counter++;
    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    // get src_rank
    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    // NACKed back if target is in recovery
    if (cdh->IsInRecovery()){
      //issue NACKs back to src rank
      LOG_DEBUG("PUT_SHORT_request: rank#%d is in recovery, NACK back to src(%d)..\n", myrank(), src_rank);
      cdh->RecordBlockedThread(src_rank);
      GASNET_CHECK_RV(SHORT_REP(4,7,(token, CD_GASNET_PUT_REPLY_NACK_LONG, nbytes, PACK(dest), PACK(org_src), PACK(done))));
      return;
    }

    uint32_t sc = cdh->GetSyncCounter();

    //gasnet_get_bulk(dest, src_rank, src, nbytes);
    LOG_DEBUG("\x1b[34m inside gasnet_put_request_short_inner, record rmda to rank#%d\x1b[0m\n", src_rank);
    cd::rdma_op_t tmp_rdma_op(src_rank, src, nbytes, dest, true/*isget*/, done, sc, old_iwc, org_src);
    //printf("\x1b[31m rdma_get::src_rank=%d,src=%p,nbytes=%d,dest=%p,isget=%d,done=%p\n\x1b[0m",
    //          tmp_rdma_op.thread_id, tmp_rdma_op.src, tmp_rdma_op.nbytes, tmp_rdma_op.dest, tmp_rdma_op.isget, tmp_rdma_op.done);
    cd::rdma_ops_.push_back(tmp_rdma_op);

    //record srcs for incoming writes to request write logs when failure happens
    cdh->RecordIncomingSrc(src_rank);
    
    LOG_DEBUG("cd_gasnet_put_request_short_inner for %p (%u) is DONE with sc %u and iwc %u\n", src, src_rank, sc, old_iwc);
    return;
  }
  SHORT_HANDLER(cd_gasnet_put_request_short, 5, 9,
               (token, UNPACK(a0),      a1, UNPACK(a2),     UNPACK(a3),     UNPACK(a4)),
               (token, UNPACK2(a0, a1), a2, UNPACK2(a3,a4), UNPACK2(a5,a6), UNPACK2(a7,a8)));
  
  // request handler for gasnet_put
  GASNETT_INLINE(cd_gasnet_put_request_inner)
  void cd_gasnet_put_request_inner(gasnet_token_t token, void* src, size_t nbytes, void* dest, void* org_src, void* done){
    uint32_t old_iwc = incoming_write_counter++;
    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    // get src_rank
    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    // NACKed back if target is in recovery
    if (cdh->IsInRecovery()){
      //issue NACKs back to src rank
      LOG_DEBUG("PUT_request: rank#%d is in recovery, NACK back to src(%d)..\n", myrank(), src_rank);
      cdh->RecordBlockedThread(src_rank);
      GASNET_CHECK_RV(SHORT_REP(4,7,(token, CD_GASNET_PUT_REPLY_NACK, nbytes, PACK(dest), PACK(org_src), PACK(done))));
      return;
    }

    uint32_t sc = cdh->GetSyncCounter();
    memcpy(dest, src, nbytes);

    //issue AMs back to src rank
    LOG_DEBUG("\x1b[34m inside gasnet_PUT_request_inner, return to rank#%d\x1b[0m\n", src_rank);
    GASNET_CHECK_RV(SHORT_REP(5,8,(token, CD_GASNET_PUT_REPLY, 
                                   sc, old_iwc, PACK(org_src), PACK(dest), PACK(done))));

    //record srcs for incoming writes to request write logs when failure happens
    cdh->RecordIncomingSrc(src_rank);
    
    LOG_DEBUG("cd_gasnet_PUT_request_inner for %p (%u) is DONE with sc %u and iwc %u\n", src, src_rank, sc, old_iwc);
    return;
  }
  MEDIUM_HANDLER(cd_gasnet_put_request, 3, 6,
                 (token, addr, nbytes, UNPACK(a0),      UNPACK(a1),      UNPACK(a2)),
                 (token, addr, nbytes, UNPACK2(a0, a1), UNPACK2(a2, a3), UNPACK2(a4,a5)));
  
  GASNETT_INLINE(cd_gasnet_put_reply_nack_long_inner)
  void cd_gasnet_put_reply_nack_long_inner(gasnet_token_t token, size_t nbytes, void* dest, void* org_src, void* done){
    CDHandle * cdh = GetCurrentCD();
    assert(cdh != NULL);

    gasnet_node_t tgt_rank;
    gasnet_AMGetMsgSource(token, &tgt_rank);

    LOG_DEBUG("inside put_reply_nack_long_inner due to task %d\n", tgt_rank);
    
    // Record Nacked AM message, and increment block_counter_
    bool tgt_recorded = cdh->RecordNackedAM(tgt_rank, org_src, nbytes, dest, 1/*islongam*/, 0/*isget*/, done);

    //blocking src threads from progressing...
    if (!blocked || !tgt_recorded){
      blocked++;
      LOG_DEBUG("\x1b[34m gasnet_PUT_reply_nack_long::blocked increased to %d due to failed target#%d\x1b[0m\n", blocked, tgt_rank);
    }

    return;
  }
  SHORT_HANDLER(cd_gasnet_put_reply_nack_long, 4, 7, 
                (token, a0, UNPACK(a1),     UNPACK(a2),     UNPACK(a3)), 
                (token, a0, UNPACK2(a1,a2), UNPACK2(a3,a4), UNPACK2(a5,a6)));

  // long request handler for gasnet_put
  GASNETT_INLINE(cd_gasnet_put_request_long_inner)
  void cd_gasnet_put_request_long_inner(gasnet_token_t token, void* src, size_t nbytes, void* dest, void* org_src, void* done){
    //SZNOTE: LONG_SEQ's payload is copied to dest before handler runs
    uint32_t old_iwc = incoming_write_counter++;
    gasnet_node_t src_rank;
    gasnet_AMGetMsgSource(token, &src_rank);

    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    // NACKed back if target is in recovery
    if (cdh->IsInRecovery()){
      //issue NACKs back to src rank
      LOG_DEBUG("PUT_LONG_request: rank#%d is in recovery, NACK back to src(%d)..\n", myrank(), src_rank);
      cdh->RecordBlockedThread(src_rank);
      GASNET_CHECK_RV(SHORT_REP(4,7,(token, CD_GASNET_PUT_REPLY_NACK_LONG, nbytes, PACK(dest), PACK(org_src), PACK(done))));
      return;
    }

    uint32_t sc = cdh->GetSyncCounter();

    //issue AMs back to src rank
    LOG_DEBUG("\x1b[34m inside gasnet_PUT_request_LONG_inner, return to rank#%d\x1b[0m\n", src_rank);
    GASNET_CHECK_RV(SHORT_REP(5,8,(token, CD_GASNET_PUT_REPLY, 
                                   sc, old_iwc, PACK(org_src), PACK(dest), PACK(done))));

    //record srcs for incoming writes to request write logs when failure happens
    cdh->RecordIncomingSrc(src_rank);
    
    LOG_DEBUG("cd_gasnet_PUT_request_long_inner for %p (%u) is DONE with sc %u and iwc %u\n", src, src_rank, sc, old_iwc);
    return;
  }
  LONG_HANDLER(cd_gasnet_put_request_long, 3, 6,
              (token, addr, nbytes, UNPACK(a0),     UNPACK(a1),     UNPACK(a2)),
              (token, addr, nbytes, UNPACK2(a0,a1), UNPACK2(a2,a3), UNPACK2(a4,a5)));
  
  // SZNOTE: assuming nbytes do not exceed AMMedium message size, need a safe guard?
  void cd_gasnet_put(gasnet_node_t node, void *dest, void *src, size_t nbytes, int skip_waiting, size_t* done)
  {
    if (app_side == false)
    {
      gasnet_put(node, dest, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_put(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }
  
    if (node == myrank()) {
      LOG_DEBUG("gasnet_put to self..\n");
      gasnet_put(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }

    int alloc_for_done = 0;
    if (done==NULL){
      done = new size_t;
      alloc_for_done = 1;
    }
    *done = 0;

    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        //gasnet_put(node, dest, src, nbytes);
        GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
                                          src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
        if (!skip_waiting)
          cd_gasnet_wait_for_done(done);
        break;
  
      case kRelaxedCDGen:
        {
          if (!skip_waiting){
            LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
            cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
            cur_cd->LogData(dest, 0);
          }
          GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
                                            src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          if (!skip_waiting)
            cd_gasnet_wait_for_done(done);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, issued remote writes are squashed in reexecution...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, 0);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            if (!skip_waiting){
              cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
              cur_cd->LogData(dest, 0);
            }
            GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
                                              src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            if (!skip_waiting)
              cd_gasnet_wait_for_done(done);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    if (alloc_for_done)
      delete done;
    app_side = local_appside;
  }
  
  void cd_gasnet_put_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes, int skip_waiting, size_t* done)
  {
    if (app_side == false)
    {
      gasnet_put_bulk(node, dest, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_put_bulk(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }
  
    if (node == myrank()) {
      LOG_DEBUG("gasnet_put_bulk to self..\n");
      gasnet_put_bulk(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }

    int alloc_for_done = 0;
    if (done==NULL){
      done = new size_t;
      alloc_for_done = 1;
    }
    *done = 0;
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        {
          //if (nbytes<max_am_payload_size()) {
          //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
          //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          //}
          //else 
          if (nbytes<max_am_request_size_long()) {
            LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                              src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
          }
          else{
            LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                        nbytes, max_am_reply_size_long());
            GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                              PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          }

          // wait for ACK
          if (!skip_waiting)
            cd_gasnet_wait_for_done(done);
        }
        break;
  
      case kRelaxedCDGen:
        {
          // log write data
          if (!skip_waiting){
            LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
            cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
            cur_cd->LogData(dest, 0);
          }

          // create OP
          //if (nbytes<max_am_payload_size()) {
          //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
          //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          //}
          //else 
          if (nbytes<max_am_request_size_long()) {
            LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                              src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
          }
          else{
            LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                        nbytes, max_am_reply_size_long());
            GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                              PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          }

          // wait for ACK
          if (!skip_waiting)
            cd_gasnet_wait_for_done(done);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, issued remote writes are squashed in reexecution...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, 0);
          if (ret == kCommLogCommLogModeFlip)
          {
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
            cur_cd->LogData(dest, 0);

            // create OP
            //if (nbytes<max_am_payload_size()) {
            //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
            //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            //}
            //else 
            if (nbytes<max_am_request_size_long()) {
              LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
              GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                                src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
            }
            else{
              LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                          nbytes, max_am_reply_size_long());
              GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                                PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            }

            // wait for ACK
            if (!skip_waiting)
              cd_gasnet_wait_for_done(done);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    if (alloc_for_done)
      delete done;
    app_side = local_appside;
  }


  gasnet_handle_t cd_gasnet_put_nb_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes, size_t* done)
  {
    if (app_side == false)
    {
      return gasnet_put_nb_bulk(node, dest, src, nbytes);
    }
  
    bool local_appside = app_side;
    app_side = false;
    gasnet_handle_t tmp_h;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      tmp_h = gasnet_put_nb_bulk(node, dest, src, nbytes);
      app_side = local_appside;
      return tmp_h;
    }
  
    if (node == myrank()) {
      LOG_DEBUG("gasnet_put_nb_bulk to self..\n");
      tmp_h = gasnet_put_nb_bulk(node, dest, src, nbytes);
      app_side = local_appside;
      return tmp_h;
    }

    assert(done == NULL);
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        {
          done = new size_t;
          *done = 0;
          //if (nbytes<max_am_payload_size()) {
          //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
          //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          //}
          //else 
          if (nbytes<max_am_request_size_long()) {
            LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                              src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
          }
          else{
            LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                        nbytes, max_am_reply_size_long());
            GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                              PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          }
        }
        break;
  
      case kRelaxedCDGen:
        {
          done = new size_t;
          *done = 0;
          // log write data
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
          cur_cd->LogData(dest, 0);

          // create OP
          //if (nbytes<max_am_payload_size()) {
          //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
          //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          //}
          //else 
          if (nbytes<max_am_request_size_long()) {
            LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                              src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
          }
          else{
            LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                        nbytes, max_am_reply_size_long());
            GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                              PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          }
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, issued remote writes are squashed in reexecution...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, 0);
          if (ret == kCommLogCommLogModeFlip)
          {
            done = new size_t;
            *done = 0;
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
            cur_cd->LogData(dest, 0);

            // create OP
            //if (nbytes<max_am_payload_size()) {
            //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
            //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            //}
            //else 
            if (nbytes<max_am_request_size_long()) {
              LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
              GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                                src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
            }
            else{
              LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                          nbytes, max_am_reply_size_long());
              GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                                PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            }
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return (gasnet_handle_t) done;
  }


  void cd_gasnet_put_nbi_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes, size_t* done){
    if (app_side == false)
    {
      gasnet_put_nbi_bulk(node, dest, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_put_nbi_bulk(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }
  
    // local put_nbi_bulk; this is a weird usage case..
    if (node == myrank()) {
      WARNING_MESSAGE("gasnet_put_nbi_bulk to self..\n");
      gasnet_put_nbi_bulk(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }

    assert(done==NULL);
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        {
          done = new size_t;
          *done = 0;
          //if (nbytes<max_am_payload_size()) {
          //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
          //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          //}
          //else 
          if (nbytes<max_am_request_size_long()) {
            LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                              src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
          }
          else{
            LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                        nbytes, max_am_reply_size_long());
            GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                              PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          }
          // record outstanding NBI messages
          cd_outstanding_nbi.push_back((void*)done);
        }
        break;
  
      case kRelaxedCDGen:
        {
          done = new size_t;
          *done = 0;
          // log write data
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
          cur_cd->LogData(dest, 0);

          // create OP
          //if (nbytes<max_am_payload_size()) {
          //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
          //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          //}
          //else 
          if (nbytes<max_am_request_size_long()) {
            LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                              src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
          }
          else{
            LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                        nbytes, max_am_reply_size_long());
            GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                              PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
          }
          // record outstanding NBI messages
          cd_outstanding_nbi.push_back((void*)done);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, issued remote writes are squashed in reexecution...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, 0);
          if (ret == kCommLogCommLogModeFlip)
          {
            done = new size_t;
            *done = 0;
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
            cur_cd->LogData(dest, 0);

            // create OP
            //if (nbytes<max_am_payload_size()) {
            //  LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            //  GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
            //                                    src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            //}
            //else 
            if (nbytes<max_am_request_size_long()) {
              LOG_DEBUG("choose AMLong due to nbytes(%ld) > max_am_payload(%ld)\n", nbytes, max_am_payload_size());
              GASNET_CHECK_RV(LONG_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST_LONG,
                                                src, nbytes, dest, PACK(dest), PACK(src), PACK((void*)done))));
            }
            else{
              LOG_DEBUG("\x1b[31m payload size (%d B) is larger than AM_LONG_MAX(%d B), so using RDMA for puts\n\x1b[0m", 
                          nbytes, max_am_reply_size_long());
              GASNET_CHECK_RV(SHORT_REQ(5, 9, (node, CD_GASNET_PUT_REQUEST_SHORT,
                                                PACK(src), nbytes, PACK(dest), PACK(src), PACK((void*)done))));
            }
            // record outstanding NBI messages
            cd_outstanding_nbi.push_back((void*)done);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return;
  }

  void cd_gasnet_put_nbi(gasnet_node_t node, void *dest, void *src, size_t nbytes, size_t* done){
    if (app_side == false)
    {
      gasnet_put_nbi(node, dest, src, nbytes);
      return;
    }
  
    bool local_appside = app_side;
    app_side = false;
    CDHandle * cur_cd = GetCurrentCD();
    if (cur_cd == NULL)
    {
      gasnet_put_nbi(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }
  
    // local put_nbi_bulk; this is a weird usage case..
    if (node == myrank()) {
      WARNING_MESSAGE("gasnet_put_nbi to self..\n");
      gasnet_put_nbi(node, dest, src, nbytes);
      app_side = local_appside;
      return;
    }

    assert(done==NULL);
  
    switch (cur_cd->GetCDLoggingMode())
    {
      case kStrictCD:
        {
          done = new size_t;
          *done = 0;
          LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
                                            src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));

          // record outstanding NBI messages
          cd_outstanding_nbi.push_back((void*)done);
        }
        break;
  
      case kRelaxedCDGen:
        {
          done = new size_t;
          *done = 0;
          // log write data
          LOG_DEBUG("In kGenerateLog mode, generating new logs...\n");
          cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
          cur_cd->LogData(dest, 0);

          // create OP
          LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
          GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
                                            src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));

          // record outstanding NBI messages
          cd_outstanding_nbi.push_back((void*)done);
        }
        break;
  
      case kRelaxedCDRead:
        {
          LOG_DEBUG("In kReplay mode, issued remote writes are squashed in reexecution...\n");
          CommLogErrT ret = cur_cd->ReadData(dest, 0);
          if (ret == kCommLogCommLogModeFlip)
          {
            done = new size_t;
            *done = 0;
            LOG_DEBUG("Reached end of logs, and begin to generate logs...\n");
            cur_cd->LogWriteData(dest, nbytes, node, INVALID_COUNTER, INVALID_COUNTER, src);
            cur_cd->LogData(dest, 0);

            // create OP
            LOG_DEBUG("choose AMMediium due to nbytes(%ld) <= max_am_payload(%ld)\n", nbytes, max_am_payload_size());
            GASNET_CHECK_RV(MEDIUM_REQ(3, 6, (node, CD_GASNET_PUT_REQUEST,
                                              src, nbytes, PACK(dest), PACK(src), PACK((void*)done))));

            // record outstanding NBI messages
            cd_outstanding_nbi.push_back((void*)done);
          }
        }
        break;
  
      default:
        ERROR_MESSAGE("Wrong number for enum CDLoggingMode (%d)!\n", cur_cd->GetCDLoggingMode());
        break;
    }
  
    app_side = local_appside;
    return;
  }


  void cd_unpack_write_logs(void* addr, size_t nbytes, size_t dst_rank, void* done) {
    char * buffer = (char*) addr;
    size_t* done_flag = (size_t*) done;

    CDHandle * cdh = GetCurrentCD();
    assert(cdh!=NULL);

    //unpack write logs and combine write log data with their corresponding SC/IWC counters
    cdh->UnpackWriteLogs(buffer, nbytes, dst_rank);
    (*done_flag)++;

    // free allocated gasnet segments
    gasnet_seg_free(addr);
    return;
  }

  GASNETT_INLINE(cd_gasnet_free_temp_buffer_inner)
  void cd_gasnet_free_temp_buffer_inner(gasnet_token_t token, void* src){
    gasnet_seg_free(src);
    return;
  }
  SHORT_HANDLER(cd_gasnet_free_temp_buffer, 1, 2,
               (token, UNPACK(a0)),
               (token, UNPACK2(a0, a1)));

  void cd_issue_rdma_ops_for_cd(void){ 
    // used to issue RDMA ops when payloads exceed AM_REQ_LONG size..
    //if (!blocked){
      //SZNOTE: need to copy to a local version because cd::rdma_ops_for_cd_ may be modified during looping over
      std::vector<cd::rdma_op_t> rdma_ops_for_cd_local(cd::rdma_ops_for_cd_);
      cd::rdma_ops_for_cd_.clear();

      for (std::vector<cd::rdma_op_t>::const_iterator ii=rdma_ops_for_cd_local.begin(); ii!=rdma_ops_for_cd_local.end(); ii++){
        //printf("\x1b[31m issue_rdma_ops_for_cd_::src_rank=%d,src=%p,nbytes=%d,dest=%p,isget=%d,done=%p\n\x1b[0m",
        //          ii->thread_id, ii->src, ii->nbytes, ii->dest, ii->isget, ii->done);
        if (ii->isget){ 
          // use gasnet_get_bulk to retrieve logs 
          gasnet_get_bulk(ii->dest, ii->thread_id, ii->src, ii->nbytes);

          // let target to free temp buffer
          GASNET_CHECK_RV(SHORT_REQ(1, 2, (ii->thread_id, CD_GASNET_FREE_TEMP_BUFFER, PACK(ii->src))));

          cd_unpack_write_logs(ii->dest, ii->nbytes, ii->thread_id, ii->done);
        }
        else {  
          ERROR_MESSAGE("Possible BUGS!! No correct path should reach here..\n");
        }
      }

      rdma_ops_for_cd_local.clear();
    //}
  }

  void cd_issue_rdma_ops(void) { 
    // used to issue RDMA ops when payloads exceed AM_REQ_LONG size..
    //if (!blocked){
      //SZNOTE: need to copy to a local version because cd::rdma_ops_ may be modified during looping over
      std::vector<cd::rdma_op_t> rdma_ops_local(cd::rdma_ops_);
      cd::rdma_ops_.clear();

      for (std::vector<cd::rdma_op_t>::const_iterator ii=rdma_ops_local.begin(); ii!=rdma_ops_local.end(); ii++){
        //printf("\x1b[31m issue_rdma_ops_::src_rank=%d,src=%p,nbytes=%d,dest=%p,isget=%d,done=%p\n\x1b[0m",
        //          ii->thread_id, ii->src, ii->nbytes, ii->dest, ii->isget, ii->done);
        if (ii->isget){ // for puts, but use gasnet_get_bulk
          gasnet_get_bulk(ii->dest, ii->thread_id, ii->src, ii->nbytes);
          GASNET_CHECK_RV(SHORT_REQ(5,8,(ii->thread_id, CD_GASNET_MARK_DONE_PUT, 
                                         ii->sc, ii->iwc, PACK(ii->org_src), PACK(ii->dest), PACK(ii->done))));
        }
        else { // for gets, but use gasnet_put_bulk
          gasnet_put_bulk(ii->thread_id, ii->dest, ii->src, ii->nbytes);
          GASNET_CHECK_RV(SHORT_REQ(1,2,(ii->thread_id, CD_GASNET_MARK_DONE_GET, PACK(ii->done))));
        }
      }

      rdma_ops_local.clear();
    //}
  }

  void cd_reissue_puts_gets(void){
    if (blocked){
      // re-issue all failed gasnet_put
      if (!cd::nacked_ams_.empty()){
        //std::cout << "Before re-issue nacked_ams_ (size=" << cd::nacked_ams_.size() << ").." << std::endl;
        for (std::list<cd::nacked_am_t>::iterator ii=cd::nacked_ams_.begin(); ii!=cd::nacked_ams_.end();) {
          if (cd::released_threads_.find(ii->thread_id)!=cd::released_threads_.end()){
            if (ii->isget){ // re-issue gets
              if (ii->islongam){
                // issue AMMedium
                LOG_DEBUG("re-issue get_bulk: target::%d, dest::%p, src::%p, nbytes::%d, done::%p\n", 
                            ii->thread_id, ii->dest, ii->src, ii->nbytes, ii->done);
                bool tmp_as = cd::app_side;
                cd::app_side=true;
                //SZ: logging of gets happen after gets come back successfully, so no need to skip at re-issue
                cd_gasnet_get_bulk(ii->dest, ii->thread_id, ii->src, ii->nbytes, 1/*skip_waiting*/, (size_t*)ii->done);
                cd::app_side=tmp_as;
              }
              else {
                // issue AMMedium
                LOG_DEBUG("re-issue get: target::%d, dest::%p, src::%p, nbytes::%d, done::%p\n", 
                            ii->thread_id, ii->dest, ii->src, ii->nbytes, ii->done);
                bool tmp_as = cd::app_side;
                cd::app_side=true;
                //SZ: logging of gets happen after gets come back successfully, so no need to skip at re-issue
                cd_gasnet_get(ii->dest, ii->thread_id, ii->src, ii->nbytes, 1/*skip_waiting*/, (size_t*)ii->done);
                cd::app_side=tmp_as;
              }
            }
            else { // re-issue puts
              if (ii->islongam){
                // issue cd_gasnet_put_bulk
                LOG_DEBUG("re-issue put_bulk: target::%d, dest::%p, src::%p, nbytes::%d\n", ii->thread_id, ii->dest, ii->src, ii->nbytes);
                bool tmp_as = cd::app_side;
                cd::app_side=true;
                // re-issue NACKed remote writes, but skip local write logging
                cd_gasnet_put_bulk(ii->thread_id, ii->dest, ii->src, ii->nbytes, 1/*skip_waiting*/, (size_t*)ii->done);
                cd::app_side=tmp_as;
              }
              else {
                // issue AMMedium
                LOG_DEBUG("re-issue put: target::%d, dest::%p, src::%p, nbytes::%d\n", ii->thread_id, ii->dest, ii->src, ii->nbytes);
                bool tmp_as = cd::app_side;
                cd::app_side=true;
                cd_gasnet_put(ii->thread_id, ii->dest, ii->src, ii->nbytes, 1/*skip_waiting*/, (size_t*)ii->done);
                cd::app_side=tmp_as;
              }
            }
            cd::blocking_threads_.erase(ii->thread_id);
            ii = cd::nacked_ams_.erase(ii);
          }
          else {
            ii++;
          }
        } // end of for
      }

      assert(blocked>=cd::released_threads_.size());
      blocked-=cd::released_threads_.size();
    //#if CD_DEBUG_ENABLED
    //  LOG_DEBUG("\x1b[34m blocked reduces %d to %d due to rank: ", cd::released_threads_.size(), blocked);
    //  for (std::unordered_set<size_t>::iterator ii=cd::released_threads_.begin(); ii!=cd::released_threads_.end();ii++)
    //    LOG_DEBUG("#%d ", *ii);
    //  LOG_DEBUG("\x1b[0m\n");
    //#endif
      cd::released_threads_.clear();
    }
    else {
      // SZNOTE: this can occur because blocked is not incremented if blocked by same target..
      assert(cd::blocking_threads_.empty());
    #if CD_DEBUG_ENABLED
      WARNING_MESSAGE("blocked is 0 at release_blocked_thread_request_inner..\n");
    #endif
      cd::released_threads_.clear();
    }
  }

  //cd::CDErrT cd_preserve(cd::CDHandle *cd_handle,
  //                       global_ptr<void *>data_ptr, 
  //                       uint64_t len, 
  //                       uint32_t preserve_mask, 
  //                       const char *my_name, 
  //                       const char *ref_name, 
  //                       uint64_t ref_offset, 
  //                       const cd::RegenObject *regen_object, 
  //                       cd::PreserveUseT data_usage)
  //{
  //  if (data_ptr.where()==myrank()){
  //    return cd_handle->Preserve(*(data_ptr.raw_ptr()), len, preserve_mask, my_name, ref_name, ref_offset, regen_object, data_usage);
  //  }
  //  else {
  //    ERROR_MESSAGE("Not Implemented..\n");
  //  }
  //}

}

#endif
