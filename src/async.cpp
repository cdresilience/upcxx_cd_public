#include "upcxx/async.h"
#include "upcxx/active_coll.h"

namespace upcxx
{
  template<>
  void gasnet_launcher<rank_t>::launch(generic_fp fp,
                                       void *async_args,
                                       size_t arg_sz,
                                       void *fu_ptr)
  {
    async_task task;
    task.init_async_task(global_myrank(),
                         _there,
                         _ack,
                         fp,
                         arg_sz,
                         async_args,
                         fu_ptr);
    submit_task(&task, _after);
  }

  template<>
  void upcxx::gasnet_launcher<range>::launch(generic_fp fp,
                                             void *async_args,
                                             size_t arg_sz,
                                             void *fu_ptr)
  {
#if 1
    for (int i = 0; i < _there.count(); i++) {
      async_task task;
      task.init_async_task(global_myrank(),
                           _there[i],
                           _ack,
                           fp,
                           arg_sz,
                           async_args,
                           fu_ptr);
      submit_task(&task, _after);

    }
#else
    if (_ack != NULL) {
      if (_there.contains(global_myrank()))
        _ack->incref(_there.count()-1);
      else
        _ack->incref(_there.count());
    }
    am_bcast(_there, _ack, fp, arg_sz, async_args, _after, global_myrank());
#endif
  }
}
